﻿using System;
using System.IO;
using Hms.Project.Data;
using Hms.Project.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Hms.Project.Seeding
{
    class Program
    {
        public static IConfigurationRoot ConfigurationRoot;

        static void Main(string[] args)
        {
            var services = new ServiceCollection();

            ConfigurationRoot = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            services.AddDbContext<DataContext>(m =>
                m.UseMySql(ConfigurationRoot.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<DataContext>()
                .AddDefaultTokenProviders();

            var provider = services.BuildServiceProvider();

            var dataContext = provider.GetRequiredService<DataContext>();
            var userManager = provider.GetRequiredService<UserManager<User>>();
            var roleManager = provider.GetRequiredService<RoleManager<Role>>();

            DataSeed.SeedData(dataContext, userManager, roleManager).Wait();
        }
    }
}
