using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Hms.Project.Api.Helpers;
using Hms.Project.Data;
using Hms.Project.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;

namespace Hms.Project.Seeding
{
    public class DataSeed
    {
        private static Random _random = new Random();

        public static async Task SeedData(DataContext dataContext, UserManager<User> userManager,
            RoleManager<Role> roleManager)
        {
            await SeedRoles(roleManager);
            if (!EnumerableExtensions.Any(dataContext.Users))
            {
                SeedUsers(userManager);
            }
        }

        private static void SeedUsers(UserManager<User> userManager)
        {
            var adminUser = new User
            {
                UserName = "Admin",
                EmailConfirmed = true,
            };

            var result = userManager.CreateAsync(adminUser, "Hms@123").Result;

            if (result.Succeeded)
            {
                userManager.AddToRoleAsync(adminUser,
                    "Administrator").Wait();
            }

            var managerUser = new User
            {
                UserName = "Manager",
                EmailConfirmed = true,
            };

            result = userManager.CreateAsync(managerUser, "Hms@123").Result;

            if (result.Succeeded)
            {
                userManager.AddToRoleAsync(managerUser,
                    "Manager").Wait();
            }
        }

        private static async Task SeedRoles(RoleManager<Role> roleManager)
        {
            if (!roleManager.RoleExistsAsync("Administrator").Result)
            {
                var role = new Role
                {
                    Name = "Administrator",
                    Id = Guid.Parse("00000000-0000-0000-0000-000000000001"),
                };

                var createResult = await roleManager.CreateAsync(role);
                if (createResult.Succeeded)
                {
                    await roleManager.AddClaimAsync(role,
                        new Claim(CustomClaimTypes.Permission, Permissions.UserCreate));
                    await roleManager.AddClaimAsync(role,
                        new Claim(CustomClaimTypes.Permission, Permissions.UserView));
                    await roleManager.AddClaimAsync(role,
                        new Claim(CustomClaimTypes.Permission, Permissions.UserUpdate));
                    await roleManager.AddClaimAsync(role,
                        new Claim(CustomClaimTypes.Permission, Permissions.UserDelete));
                }
            }

            if (!roleManager.RoleExistsAsync("Manager").Result)
            {
                var role = new Role
                {
                    Name = "Manager",
                    Id = Guid.Parse("00000000-0000-0000-0000-000000000002"),
                };
                var createResult = await roleManager.CreateAsync(role);
                if (createResult.Succeeded)
                {
                    await roleManager.AddClaimAsync(role,
                        new Claim(CustomClaimTypes.Permission, Permissions.UserView));
                }
            }
        }
    }
}