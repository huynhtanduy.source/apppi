namespace Hms.Project.Service.Models
{
    public class RecaptchaSettings
    {
        public string Endpoint { get; set; }
        public string SiteKey { get; set; }
        public string SecretKey { get; set; }
    }
    public class AzureBlobStorageSettings
    {
        public string Blob_StorageAccount { get; set; }
        public string Blob_StorageKey { get; set; }
        public string Blob_ContainerName { get; set; }
    }
    public class JWTSettings
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public int ExpireDays { get; set; } = 30;
        public int LoginMaxTry { get; set; } = 5;
    }
    public class LanguageFileSettings
    {
        public string Url { get; set; }
        public string Name { get; set; }
    }
    public class RateLimitSetings
    {
        public long Time { get; set; }
        public long Count { get; set; }
    }

    public class OTPSettings
    {
        public bool SendByEmail { get; set; }
        public bool LoginWithTwoFactorAuthenticator { get; set; }
        public int ExpireMinutes { get; set; }
        public int CodeSize { get; set; }
    }
    public class AppSettings
    {
        public bool IsWriteLog { get; set; }
        public int TokenLifeTime { get; set; } = 900;
        public int RefreshTokenLifeTime { get; set; } = 1800;
        public JWTSettings JWT { get; set; }
        public AzureBlobStorageSettings AzureBlobStorage { get; set; }
        public RecaptchaSettings Recaptcha { get; set; }
        public LanguageFileSettings LanguageFile { get; set; }
        public RateLimitSetings RateLimit { get; set; }
        public OTPSettings OTP { get; set; }
        public string TemplateUrl { get; set; }
        public string ApiBasicUrl { get; set; }

    }
}