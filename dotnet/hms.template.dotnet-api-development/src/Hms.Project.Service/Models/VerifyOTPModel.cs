using System;

namespace Hms.Project.Service.Models
{
    public class VerifyOTPModel
    {
        public Guid VerifyCodeId { get; set; }
        public string VerifyCode { get; set; }
        public string DeviceId { get; set; }
        public string ClientIp { get; set; }
    }
}