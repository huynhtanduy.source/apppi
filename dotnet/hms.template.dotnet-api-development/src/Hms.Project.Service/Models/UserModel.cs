using System;
using System.Collections.Generic;
using Hms.Project.Data.Entities;
using Newtonsoft.Json;

namespace Hms.Project.Service.Models
{
    public class UserModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string FirstName { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string LastName { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string FullName { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Email { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string PhoneNumber { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Avatar { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string AvatarUrl { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid? RoleId { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string RoleName { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string AccessToken { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string TokenSession { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<string> Permissions { get; set; }
        public bool Deactivated { get; set; }
        public bool IsLocked { get; set; }
    }

    public class UserRequestModel
    {
        public User User { get; set; }
        public string Password { get; set; }
        public Guid RoleId { get; set; }
        public bool IsSystemUser { get; set; }
        public bool IsLocked { get; set; }
    }

    public class ProfileRequestModel : UserRequestModel
    {
        public string OldPassword { get; set; }
    }

    public class TokenModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime Expires { get; set; }
        public Guid UserId { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int TokenLifeTime { get; set; }
    }

    public class SignInModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ClientIp { get; set; }
        public string DeviceId { get; set; }
    }
    
    public class SignInWithTwoFactorModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}