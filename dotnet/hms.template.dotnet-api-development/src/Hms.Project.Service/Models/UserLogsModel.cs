﻿using System;

namespace Hms.Project.Service.Models
{
    public class UserLogsModel
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid? UserId { get; set; }
        public string ClientIp { get; set; }
        public string Link { get; set; }
        public int Type { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    }
}
