namespace Hms.Project.Service.Models
{
    public class GetAuditDetailsResultModel: BaseModel
    {
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string TableName { get; set; }
    }
}