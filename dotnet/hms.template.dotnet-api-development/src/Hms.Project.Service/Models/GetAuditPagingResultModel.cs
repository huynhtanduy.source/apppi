using System;
using Hms.Project.Data.Entities;

namespace Hms.Project.Service.Models
{
    public class GetAuditPagingResultModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Action { get; set; }
        public string ColumnNames { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}