﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Hms.Project.Service.Models
{
    public class BaseModel
    {
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Creator { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime CreatedDate { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string LatestModifier { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime? ModifiedDate { get; set; }
    }
}
