﻿namespace Hms.Project.Service.Models
{
    public class LanguageItemModel
    {
        public string Id { get; set; }
        public string ValueEN { get; set; }
        public string ValueVN { get; set; }
    }

    public class LanguageModel
    {
        public string Id { get; set; }
        public string Value { get; set; }
        public string Language { get; set; }
        public string LastUpdate { get; set; }
    }
}
