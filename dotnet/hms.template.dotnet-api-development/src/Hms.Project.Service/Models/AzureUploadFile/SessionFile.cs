﻿using System;
using System.Collections.Generic;

namespace Hms.Project.Service.Models.AzureUploadFile
{
    public class SessionFile
    {
        public SessionFile(FileInformation fileInfo) : this(fileInfo, DEFAULT_TIMEOUT)
        {

        }

        public SessionFile(FileInformation fileInformation, long timeout)
        {
            this.Id = System.Guid.NewGuid().ToString();
            this.CreatedDate = DateTime.Now;
            this.LastUpdate = this.CreatedDate;
            this.FileInfo = fileInformation;
            this.Timeout = timeout;
            this.BlockIds = new List<string>();
        }

        public string Id { get; private set; }

        public DateTime CreatedDate { get; private set; }
        private bool failed = false;

        public DateTime LastUpdate { get; private set; }

        public long Timeout { get; private set; }
        public List<string> BlockIds { get; set; }

        private static long DEFAULT_TIMEOUT = 3600L;



        public double Progress
        {
            get
            {
                if (FileInfo.TotalNumberOfChunks == 0)
                    return 0;

                return SuccessfulChunks / (FileInfo.TotalNumberOfChunks * 1f);
            }
        }

        public String Status
        {
            get
            {
                if (failed)
                    return "failed";
                else if (IsConcluded)
                    return "done";

                return "ongoing";
            }
        }

        public bool IsConcluded
        {
            get
            {
                return FileInfo.TotalNumberOfChunks == FileInfo.AlreadyPersistedChunks.Count;
            }
        }


        public int SuccessfulChunks
        {
            get
            {
                return FileInfo.AlreadyPersistedChunks.Count;
            }
        }

        public bool HasFailed()
        {
            return failed;
        }

        public bool IsExpired
        {
            get
            {
                TimeSpan span = DateTime.Now - LastUpdate;
                return span.TotalSeconds >= Timeout;
            }
        }

        public void MaskAsFailed()
        {
            failed = true;
        }

        public FileInformation FileInfo
        {
            get; private set;
        }

        public void RenewTimeout()
        {
            LastUpdate = DateTime.Now;
        }
    }
}