using System.Collections.Generic;

namespace Hms.Project.Service.Models
{
    public class Paging<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long TotalCount { get; set; }
        public IEnumerable<T> Result { get; set; }

        public Paging(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalCount = 0;
            Result = null;
        }

        public Paging()
        {
        }
    }
}