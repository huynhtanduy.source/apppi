using System;
using System.Collections.Generic;

namespace Hms.Project.Service.Models
{
    public class PagingResult<T>
    {
        public Paging Paging { get; set; }
        public IEnumerable<T> Data { get; set; }

        public PagingResult()
        {
        }

        public PagingResult(int pageIndex, int pageSize)
        {
            Paging = new Paging
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalCount = 0
            };
            Data = null;
        }

        public PagingResult(IEnumerable<T> data, long totalCount, int pageIndex, int pageSize)
        {
            Paging = new Paging
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalCount = totalCount
            };
            Data = data;
        }
    }

    public class Paging
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long TotalCount { get; set; }
        public int TotalPages => PageSize > 0 ? (int)(Math.Ceiling(((decimal)TotalCount / PageSize))) : 0;
    }
}