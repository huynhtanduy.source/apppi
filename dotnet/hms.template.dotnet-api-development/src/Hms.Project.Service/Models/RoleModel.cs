﻿using System;
using System.Collections.Generic;
using System.Text;
using Hms.Project.Data.Entities;
using Newtonsoft.Json;

namespace Hms.Project.Service.Models
{
    public class RoleModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class RoleDetailModel : RoleModel
    {
        public List<string> Permissions { get; set; }
    }

    public class RoleRequestModel
    {

        public Role Role { get; set; }
        public ICollection<string> Permissions { get; set; }
    }
}
