﻿namespace Hms.Project.Service.Constants
{
    public class CommonConst
    {
        //Text Resource
        public const string ResourceVersionKey = "_version";
        public const string ResourceSpecialKey = "@_update_date_version_key";
    }
    public class LanguageConst
    {
        public const string LanguageVN = "vn";
        public const string LanguageEN = "en";

    }
}
