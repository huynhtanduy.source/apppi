namespace Hms.Project.Service.Constants
{
    public static class AuditActionType
    {
        public const string Insert = "Insert";
        public const string Update = "Update";
        public const string Delete = "Delete";
    }
}