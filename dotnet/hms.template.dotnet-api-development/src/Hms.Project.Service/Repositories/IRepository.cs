using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Hms.Project.Service.Repositories
{
    public interface IRepository<TEntity>
    {
        #region Get
        IQueryable<TEntity> GetQuery();

        IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> expression);
        DbContext GetDbContext();

        TEntity Single(Expression<Func<TEntity, bool>> criteria);
        Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> criteria);


        #endregion

        #region Find
        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> criteria);
        TEntity FindOne(Expression<Func<TEntity, bool>> criteria);
        Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> criteria);
        #endregion

        #region Count

        int Count();
        Task<int> CountAsync();
        int Count(Expression<Func<TEntity, bool>> criteria);
        Task<int> CountAsync(Expression<Func<TEntity, bool>> criteria);
        #endregion

        #region Update
        void Attach(TEntity entity, EntityState state = EntityState.Unchanged);
        void Update(TEntity entity, Expression<Func<TEntity, bool>> criteria);
        void Update(TEntity entity);
        #endregion

        #region Add
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        #endregion

        #region Remove
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        #endregion

        // Get the lastest value from db
        void Reload(TEntity entity);
    }
}