﻿using Hms.Project.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hms.Project.Service.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public DataContext Context { get; }

        private IDbContextTransaction _transaction = null;

        public UnitOfWork(DataContext context)
        {
            Context = context;
        }

        public IDisposable BeginTransaction()
        {
            if (_transaction != null)
                throw new Exception("Cannot begin a new transaction while an existing transaction is still running. Please commit or rollback the existing transaction before starting a new one.");

            _transaction = Context.Database.BeginTransaction();
            return (IDisposable)_transaction;
        }

        public void CommitChanges()
        {
            Context.SaveChanges();
        }

        public async Task CommitChangesAsync()
        {
            await Context.SaveChangesAsync();
        }

        public void CommitTransaction()
        {
            if (_transaction == null)
                throw new Exception("Cannot commit a transaction while there is no transaction running.");

            Context.SaveChanges();
            _transaction.Commit();
            ReleaseTransaction();
        }

        public void RollbackTransaction()
        {
            if (_transaction == null)
                throw new Exception("Cannot roll back a transaction while there is no transaction running.");

            _transaction.Rollback();
            ReleaseTransaction();
        }

        public DataContext GetContext()
        {
            return Context;
        }

        public void ReleaseTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
