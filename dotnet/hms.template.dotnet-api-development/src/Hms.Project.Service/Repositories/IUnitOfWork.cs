﻿using Hms.Project.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hms.Project.Service.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Begins the transaction.
        /// </summary>
        /// <returns></returns>
        IDisposable BeginTransaction();

        /// <summary>
        /// Commits the changes.
        /// </summary>
        void CommitChanges();

        /// <summary>
        /// Commits the changes.
        /// </summary>
        Task CommitChangesAsync();

        /// <summary>
        /// Commits the transaction.
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// Rollbacks the transaction.
        /// </summary>
        void RollbackTransaction();

        DataContext GetContext();
    }
}