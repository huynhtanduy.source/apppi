﻿namespace Hms.Project.Service.Common
{
    public class Enumerations
    {
        public enum UserLogType
        {
            Login = 1,
            Logout = 2,
            LimitIP = 3,
        }
    }
}
