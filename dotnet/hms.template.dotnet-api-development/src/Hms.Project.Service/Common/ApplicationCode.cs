﻿using System.ComponentModel;

namespace Hms.Project.Service.Common
{
    // 10000: User
    public enum UserIssue
    {
        UserExceptionCode = 10000,
        [Description("User not found or locked")]
        UserNotFound = 10001,
        [Description("User is existed")]
        UserExisted = 10002,
        [Description("Some of propertyIds are not exist or deleted")]
        UserPropertyIdsNotExist = 10003,
        [Description("Roles is missing or not exist")]
        UserRoleIdsNotExist = 10004,
        [Description("Cannot self delete")]
        UserSelfDelete = 10005,
        [Description("User will be locked after too many fail tries")]
        UserLockWarning = 10006,
        [Description("Email hadn't confirmed yet")]
        EmailNotConfirmed = 10007,
        [Description("User was locked")]
        UserLocked = 10008,
        [Description("Wrong reset code")]
        WrongResetCode = 10010,
        [Description("Wrong old password")]
        WrongOldPassword = 10011,
        [Description("Email existed")]
        EmailExisted = 10012,
        [Description("Token has expired")]
        TokenExpired = 10013,
        [Description("Invalid captcha")]
        InvalidCaptcha = 10014,
        [Description("Invalid login method")]
        InvalidLoginMethod = 10015,
        [Description("Invalid send OTP")]
        InvalidSendOTP = 10016,
        [Description("Invalid OTP code")]
        InvalidOTPCode = 10017,
    }

    public enum RoleIssue
    {
        RoleExceptionCode = 11000,
        [Description("Role not found")]
        RoleNotFound = 11001,
        [Description("Role is existed")]
        RoleExisted = 11002,
        [Description("Role is used")]
        RoleUsed = 11003,
    }


    // 12000: Common
    public enum CommonIssue
    {
        [Description("IP của bạn đã bị chặn. Vui lòng quay lại sau")]
        IPErrorCode = 12000,

    }


}