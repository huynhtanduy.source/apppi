﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Hms.Project.Common;
using Hms.Project.Service.Constants;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Hms.Project.Service.Services
{
    public class LanguageService : ILanguageService
    {
        private readonly AppSettings _appSettings;
        private readonly IHostingEnvironment _hostingEnvironment;
        private const string ID = "id";
        private const string VALUE_VN = "valuevn";
        private const string VALUE_EN = "valueen";
        private readonly string rootPath;
        public LanguageService(IOptions<AppSettings> appSettings, IHostingEnvironment hostingEnvironment)
        {
            _appSettings = appSettings.Value;
            _hostingEnvironment = hostingEnvironment;
            rootPath = $"{_hostingEnvironment.WebRootPath}{_hostingEnvironment.ContentRootPath}";
        }
        public async Task<PagingResult<LanguageItemModel>> GetAll(LanguageFilterQuery query)
        {
            try
            {
                var jsonFileVN = string.Format($"{rootPath}{_appSettings.LanguageFile.Url}{_appSettings.LanguageFile.Name}", LanguageConst.LanguageVN.ToLower());
                var jsonFileEN = string.Format($"{rootPath}{_appSettings.LanguageFile.Url}{_appSettings.LanguageFile.Name}", LanguageConst.LanguageEN.ToLower());
                Dictionary<string, object> jsonFileDataUS = GetFileDictionaryData(jsonFileEN);
                Dictionary<string, object> jsonFileDataNO = GetFileDictionaryData(jsonFileVN);
                var result = MergeDictionaries(jsonFileDataUS, jsonFileDataNO);
                result.RemoveAt(0);
                return GetLanguageFilterQuery(query, result);
            }
            catch
            {
                return new PagingResult<LanguageItemModel>();
            }
        }

        public async Task<LanguageModel> Update(LanguageModel languageModel)
        {
            try
            {
                if (languageModel == null)
                {
                    return null;
                }

                bool isUpdate = false;

                var filePath = string.Format($"{rootPath}{_appSettings.LanguageFile.Url}{_appSettings.LanguageFile.Name}", languageModel.Language.ToLower());
                var fileDataDic = GetFileDictionaryData(filePath);


                if (fileDataDic == null)
                {
                    return null;
                }

                Dictionary<string, object> root = new Dictionary<string, object>();
                fileDataDic = ReworkList(fileDataDic, languageModel.Id, languageModel.Value);

                if (fileDataDic.Any())
                {
                    UpdateSpcialKey(fileDataDic);
                    dynamic json = JsonConvert.SerializeObject(fileDataDic.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value), Formatting.Indented);
                    isUpdate = SaveResourceLanguageFile(json, filePath);
                }
                if (isUpdate)
                {
                    languageModel.LastUpdate = DateTime.Now.ToUniversalTime().ToString("u").TrimEnd('Z');
                    return languageModel;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        public string ReadJsonOject(string language)
        {
            try
            {
                var filePath = string.Format($"{rootPath}{_appSettings.LanguageFile.Url}{_appSettings.LanguageFile.Name}", language.ToLower());

                using (StreamReader r = new StreamReader(filePath))
                {
                    var json = r.ReadToEnd();
                    return json;
                }
            }
            catch
            {
                return "";
            }
        }

        #region Functions

        private PagingResult<LanguageItemModel> GetLanguageFilterQuery(LanguageFilterQuery query, List<LanguageItemModel> data)
        {
            var dataQuery = data
                .Where(p =>
                      (string.IsNullOrEmpty(query.SearchTerm)
                        || p.Id.Contains(query.SearchTerm.Trim())
                        || p.ValueVN.Contains(query.SearchTerm.Trim())
                        || p.ValueEN.Contains(query.SearchTerm.Trim()))
                );

            switch (query.OrderBy.ToLower())
            {
                case ID:
                    dataQuery = !query.OrderByDesc
                        ? dataQuery.OrderBy(p => p.Id)
                        : dataQuery.OrderByDescending(p => p.Id);
                    break;
                case VALUE_VN:
                    dataQuery = !query.OrderByDesc
                        ? dataQuery.OrderBy(p => p.ValueVN)
                        : dataQuery.OrderByDescending(p => p.ValueVN);
                    break;
                case VALUE_EN:
                    dataQuery = !query.OrderByDesc
                        ? dataQuery.OrderBy(p => p.ValueEN)
                        : dataQuery.OrderByDescending(p => p.ValueEN);
                    break;
                default:
                    dataQuery = dataQuery.OrderByDescending(p => p.Id);
                    break;
            }
            var offset = (query.PageIndex - 1) * query.PageSize;
            var limit = query.PageSize;
            var total = dataQuery.Count();
            var utcNow = DateTime.UtcNow;

            var dataResponse = dataQuery.Skip(offset).Take(limit).ToList();

            return new PagingResult<LanguageItemModel>(dataResponse, total, query.PageIndex, query.PageSize);
        }

        private List<LanguageItemModel> MergeDictionaries(Dictionary<string, object> jsonFileDataUS, Dictionary<string, object> jsonFileDataNO)
        {
            var result = new List<LanguageItemModel>();

            Dictionary<string, object> rootDataVN = new Dictionary<string, object>();
            Dictionary<string, object> rootDataEN = new Dictionary<string, object>();

            ReworkList(rootDataVN, jsonFileDataUS);
            ReworkList(rootDataEN, jsonFileDataNO);

            if (rootDataVN.Any() && rootDataEN.Any())
            {
                //Merge 2 files
                foreach (var itemVN in rootDataVN)
                {
                    var valueEN = rootDataVN.ContainsKey(itemVN.Key) ? rootDataEN[itemVN.Key].ToString() : string.Empty;

                    var textResourceItem = new LanguageItemModel
                    {
                        Id = itemVN.Key,
                        ValueEN = itemVN.Value.ToString(),
                        ValueVN = valueEN
                    };
                    result.Add(textResourceItem);
                }
            }

            return result;
        }

        private Dictionary<string, object> ReworkList(Dictionary<string, object> root, Dictionary<string, object> data, string parentKey = "")
        {
            foreach (var item in data)
            {
                var key = item.Key;
                if (!string.IsNullOrEmpty(parentKey))
                {
                    key = string.Format("{0}.{1}", parentKey, item.Key);
                }

                if (item.Value.GetType() == typeof(string))
                {
                    if (!root.ContainsKey(key))
                    {
                        root.Add(key, item.Value);
                    }
                }
                else
                {
                    dynamic json = JsonConvert.SerializeObject(item.Value);
                    var dc = JsonConvert.DeserializeObject<Dictionary<string, object>>(json, new DictionaryConverter());
                    ReworkList(root, dc, key);
                }
            }

            return root;
        }

        private Dictionary<string, object> GetFileDictionaryData(string serverFilePath)
        {
            if (System.IO.File.Exists(serverFilePath))
            {
                string dataRead = string.Empty;
                using (StreamReader sr = new StreamReader(serverFilePath))
                {
                    dataRead = sr.ReadToEnd();
                }
                return JsonConvert.DeserializeObject<Dictionary<string, object>>(dataRead, new DictionaryConverter());
            }
            return null;
        }
        private bool SaveResourceLanguageFile(string dataJson, string filePath)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.Write(dataJson);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void UpdateSpcialKey(Dictionary<string, object> source)
        {
            var updateTime = DateTime.Now.ToString("u").TrimEnd('Z');
            if (!source.ContainsKey(CommonConst.ResourceSpecialKey))
            {
                source.Add(CommonConst.ResourceSpecialKey, updateTime);
            }
            else
            {
                source[CommonConst.ResourceSpecialKey] = updateTime;
            }

        }

        private Dictionary<string, object> ReworkList(Dictionary<string, object> data, string keyToUpdate, string newValue, string parentKey = "")
        {
            var dictionary = new Dictionary<string, object>();
            var dataList = data.ToList();
            for (var i = 0; i < dataList.Count; i++)
            {
                var item = dataList[i];
                var key = item.Key;
                if (!string.IsNullOrEmpty(parentKey))
                {
                    key = string.Format("{0}.{1}", parentKey, item.Key);
                }

                if (item.Value.GetType() == typeof(string))
                {
                    dictionary.Add(item.Key, (keyToUpdate == key) ? newValue : item.Value);
                }
                else
                {
                    dynamic json = JsonConvert.SerializeObject(item.Value);
                    var dc = JsonConvert.DeserializeObject<Dictionary<string, object>>(json, new DictionaryConverter());
                    dictionary.Add(item.Key, ReworkList(dc, keyToUpdate, newValue, key));
                };
            }

            return dictionary;
        }

        #endregion Functions
    }
}
