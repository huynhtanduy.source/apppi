using System;
using System.Threading.Tasks;
using Hms.Project.Service.Interfaces;

namespace Hms.Project.Service.Services
{
    public class SendOTPService : ISendOTPService
    {
        public bool SendOTPByEmail(string email, string OTP)
        {
            Console.WriteLine(OTP);
            return true;
        }
        
        public bool SendOTPBySMS(string phoneNumber, string OTP)
        {
            Console.WriteLine(OTP);
            return true;
        }
    }
}