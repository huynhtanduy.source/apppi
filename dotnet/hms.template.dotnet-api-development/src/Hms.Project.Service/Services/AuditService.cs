using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hms.Project.Common.Extensions;
using Hms.Project.Data;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Hms.Project.Service.Services
{
    public class AuditService : IAuditService
    {
        private readonly DataContext _dataContext;

        public AuditService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public async Task <PagingResult<GetAuditPagingResultModel>> GetPaging(AuditFilterQuery query)
        {
            var auditQuery = GetAuditFilterQuery(query);
            var offset = (query.PageIndex - 1) * query.PageSize;
            var limit = query.PageSize;
            var total = await auditQuery.CountAsync();
            var auditResponse = await auditQuery.Skip(offset).Take(limit).ToListAsync();

            return new PagingResult<GetAuditPagingResultModel>(auditResponse, total, query.PageIndex, query.PageSize);
        }

        public async Task<IList<GetAuditDetailsResultModel>> GetAuditById(Guid id)
        {
            var result = new List<GetAuditDetailsResultModel>();
            var query = from au in _dataContext.Audits
                        where au.Id.Equals(id)
                        select new Audit()
                        { 
                            Id = au.Id,
                            OldValues = au.OldValues,
                            NewValues = au.NewValues,
                            TableName = au.TableName
                        };
            var queryResult = await query.FirstOrDefaultAsync();
            if (queryResult?.Id.IsValid() == true)
            {
                var sourceJObject = JsonConvert.DeserializeObject<JObject>(queryResult.OldValues);
                var targetJObject = JsonConvert.DeserializeObject<JObject>(queryResult.NewValues);

                if (!JToken.DeepEquals(sourceJObject, targetJObject))
                {
                    foreach (KeyValuePair<string, JToken> sourceProperty in sourceJObject)
                    {
                        JProperty targetProp = targetJObject.Property(sourceProperty.Key);

                        if (!JToken.DeepEquals(sourceProperty.Value, targetProp.Value))
                        {
                            result.Add(new GetAuditDetailsResultModel
                            {
                                FieldName = sourceProperty.Key,
                                OldValue = sourceProperty.Value.ToString(),
                                NewValue = targetProp.Value.ToString(),
                                TableName = queryResult.TableName
                            });
                        }
                    }
                }
            }

            return result;
        }

        private string GetJsonValueByKey(string jsonValue, string key)
        {
            if (jsonValue.IsValid() && key.IsValid())
            {
                var jObject = JsonConvert.DeserializeObject<JObject>(jsonValue);
                if (jObject != null)
                {
                    foreach (KeyValuePair<string, JToken> sourceProperty in jObject)
                    {
                        if (sourceProperty.Key.ToLower().Equals(key.ToLower()))
                        {
                            return sourceProperty.Value.ToString();
                        }
                    }
                }
            }
            return string.Empty;
        }
        
        private IQueryable<GetAuditPagingResultModel> GetAuditFilterQuery(AuditFilterQuery query)
        {
            var fromDate = query.FromDate.IsMinDate();
            var toDate = query.ToDate.IsMinDate();
            var auditQuery = from au in _dataContext.Audits
                                join user in _dataContext.Users on au.UserId equals user.Id
                                where (query.Id == Guid.Empty || au.KeyFieldId.Equals(query.Id)) && 
                                      (query.TableName == string.Empty || au.TableName.ToLower().Equals(query.TableName.ToLower())) && 
                                      ((fromDate == null && toDate == null) || (fromDate != null && toDate != null && 
                                      au.AuditDate.Date >= fromDate.Value.Date && au.AuditDate.Date <= toDate.Value.Date))
                                orderby au.AuditDate descending
                                select new GetAuditPagingResultModel()
                                {
                                    Id = au.Id,
                                    Action = au.Action,
                                    Creator = user.FullName,
                                    CreatedDate = au.AuditDate,
                                    ColumnNames = query.ColumnNames,
                                    OldValue = GetJsonValueByKey(au.OldValues, query.ColumnNames),
                                    NewValue = GetJsonValueByKey(au.NewValues, query.ColumnNames)
                                };

            var abc = from au in _dataContext.Audits
                select new GetAuditPagingResultModel()
                {
                    Id = au.Id,
                    Action = au.Action,
                    Creator = "user.FullName",
                    ColumnNames = "query.ColumnNames",
                    // OldValue = GetJsonValueByKey(au.OldValues, "query.ColumnNames"),
                    // NewValue = GetJsonValueByKey(au.NewValues, "query.ColumnNames")
                };
            return abc;
        }
    }
}