using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Hms.Project.Common;
using Hms.Project.Common.Extensions;
using Hms.Project.Data;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Common;
using Hms.Project.Service.Constants;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Hms.Project.Service.Services
{
    public class RoleService : IRoleService
    {
        private const string CUSTOM_CLAIM_TYPE = "Permission";
        private const string NAME = "name";
        private const string CREATED = "createddate";

        private readonly DataContext _dataContext;
        private readonly RoleManager<Role> _roleManager;
        private readonly IUserService _userService;

        public RoleService(DataContext dataContext,
            RoleManager<Role> roleManager,
            IUserService userService)
        {
            _dataContext = dataContext;
            _roleManager = roleManager;
            _userService = userService;
        }

        public async Task Create(RoleRequestModel roleRequestModel, Guid userId)
        {
            if (_roleManager.RoleExistsAsync(roleRequestModel.Role.Name).Result)
            {
                throw new HmsException(RoleIssue.RoleExisted);
            }

            roleRequestModel.Role.Id = Guid.NewGuid();
            roleRequestModel.Role.CreatedBy = userId;
            using (var transaction = await _dataContext.Database.BeginTransactionAsync())
            {
                try
                {
                    var createResult = await _roleManager.CreateAsync(roleRequestModel.Role);
                    if (createResult.Succeeded)
                    {
                        foreach (var permission in roleRequestModel.Permissions)
                        {
                            await _roleManager.AddClaimAsync(roleRequestModel.Role, new Claim(CUSTOM_CLAIM_TYPE, permission));
                        }
                    }
                    else
                    {
                        var error = createResult.Errors.First();
                        throw new HmsException(error.Description, error.Code);
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public async Task Update(RoleRequestModel roleRequest, Guid userId)
        {
            var roleQuery = await _roleManager.Roles.SingleOrDefaultAsync(p => p.Id.Equals(roleRequest.Role.Id));
            if (roleQuery == null)
            {
                throw new HmsNotFoundException(RoleIssue.RoleNotFound);
            }

            if (roleRequest.Role.Name != roleQuery.Name &&
                _dataContext.Roles.Any(x => x.Name == roleRequest.Role.Name))
            {
                throw new HmsException(RoleIssue.RoleExisted);
            }
            
            var auditOldValues = roleQuery.ToJson();
            var auditNewValues = roleRequest.Role.ToJson();
            roleQuery.Name = roleRequest.Role.Name;
            roleQuery.ModifiedBy = userId;
            roleQuery.ModifiedDate = DateTime.UtcNow;

            using (var transaction = await _dataContext.Database.BeginTransactionAsync())
            {
                try
                {
                    var updateResult = await _roleManager.UpdateAsync(roleQuery);
                    if (updateResult.Succeeded)
                    {
                        // Call save audit log
                        _dataContext.SaveAuditTrackingLog(new Audit
                        {
                            KeyFieldId = roleQuery.Id,
                            OldValues = auditOldValues,
                            NewValues = auditNewValues,
                            Action = AuditActionType.Update,
                            UserId = userId,
                            TableName = "Role"
                        });
                        
                        var removeClaims = _dataContext.RoleClaims.Where(p => p.RoleId == roleQuery.Id).ToList();
                        _dataContext.RoleClaims.RemoveRange(removeClaims);

                        var claims = roleRequest.Permissions
                            .Select(p => new IdentityRoleClaim<Guid>
                            {
                                RoleId = roleQuery.Id,
                                ClaimType = CUSTOM_CLAIM_TYPE,
                                ClaimValue = p,
                            });

                        _dataContext.RoleClaims.AddRange(claims);
                        _dataContext.SaveChanges();

                    }
                    else
                    {
                        var error = updateResult.Errors.First();
                        throw new HmsException(error.Description, error.Code);
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public async Task Delete(Guid id)
        {
            var roleQuery = await _roleManager.Roles.Where(p => p.Id == id)
                .FirstOrDefaultAsync();

            if (roleQuery == null)
            {
                throw new HmsNotFoundException(RoleIssue.RoleNotFound);
            }

            var userRoleQuery = await _dataContext.UserRoles.Where(p => p.RoleId == id).FirstOrDefaultAsync();
            if (userRoleQuery != null)
            {
                throw new HmsException(RoleIssue.RoleUsed);
            }

            using (var transaction = await _dataContext.Database.BeginTransactionAsync())
            {
                try
                {
                    var deleteResult = _roleManager.DeleteAsync(roleQuery).Result;
                    if (deleteResult.Succeeded)
                    {
                        var removeClaims = _dataContext.RoleClaims.Where(p => p.RoleId == roleQuery.Id).ToList();
                        _dataContext.RoleClaims.RemoveRange(removeClaims);
                    }
                    else
                    {
                        var error = deleteResult.Errors.First();
                        throw new HmsException(error.Description, error.Code);
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public async Task<RoleDetailModel> GetOne(Guid id)
        {
            var roleQuery = await _roleManager.Roles.SingleOrDefaultAsync(p => p.Id.Equals(id));
            if (roleQuery == null)
            {
                throw new HmsNotFoundException(RoleIssue.RoleNotFound);
            }

            var claims = _roleManager.GetClaimsAsync(roleQuery).Result
                .Where(c => c.Type == CUSTOM_CLAIM_TYPE)
                .Select(c => c.Value)
                .ToList();

            return new RoleDetailModel
            {
                Id = roleQuery.Id,
                Name = roleQuery.Name,
                Permissions = claims,
                Creator = _userService.GetNameById(roleQuery.CreatedBy).Result,
                CreatedDate = roleQuery.CreatedDate,
                LatestModifier = _userService.GetNameById(roleQuery.ModifiedBy.GetValueOrDefault()).Result,
                ModifiedDate = roleQuery.ModifiedDate
            };
        }

        public async Task<PagingResult<RoleModel>> GetList(RoleFilterQuery query)
        {
            var roleQuery = GetRoleFilterQuery(query);
            var offset = (query.PageIndex - 1) * query.PageSize;
            var limit = query.PageSize;
            var total = await roleQuery.CountAsync();

            var roleResponse = await roleQuery
                .Select(p => new RoleModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    CreatedDate = p.CreatedDate
                })
                .Skip(offset).Take(limit).ToListAsync();

            return new PagingResult<RoleModel>(roleResponse, total, query.PageIndex, query.PageSize);
        }

        public async Task<ICollection<RoleModel>> GetList()
        {
            var roles = await _dataContext.Roles
                .Select(p => new RoleModel
                {
                    Id = p.Id,
                    Name = p.Name
                }).OrderBy(p => p.Name).ToListAsync();

            return roles;
        }

        #region Private

        private IQueryable<Role> GetRoleFilterQuery(RoleFilterQuery query)
        {
            var roleQuery = _roleManager.Roles
                .Where(p => (string.IsNullOrEmpty(query.Name) || p.Name.Contains(query.Name.Trim()))
                    && (string.IsNullOrEmpty(query.SearchTerm) || p.Name.Contains(query.SearchTerm.Trim())));

            switch (query.OrderBy.ToLower())
            {
                case NAME:
                    roleQuery = !query.OrderByDesc
                        ? roleQuery.OrderBy(p => p.Name)
                        : roleQuery.OrderByDescending(p => p.Name);
                    break;
                case CREATED:
                    roleQuery = !query.OrderByDesc
                        ? roleQuery.OrderBy(p => p.CreatedDate)
                        : roleQuery.OrderByDescending(p => p.CreatedDate);
                    break;
                default:
                    roleQuery = roleQuery.OrderByDescending(p => p.CreatedDate);
                    break;
            }

            return roleQuery;
        }

        #endregion
    }
}