﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Hms.Project.Service.Repositories;

namespace Hms.Project.Service.Services
{
    public class UserLogsService : IUserLogsService
    {
        private readonly IRepository<UserLog> _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserLogsService(IRepository<UserLog> userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<List<UserLogsModel>> GetAll(UserLogFilterQuery filter)
        {

            var query = _userRepository.GetQuery(p => (string.IsNullOrEmpty(filter.Link) || p.Link == filter.Link)
                 && (string.IsNullOrEmpty(filter.ClientIp) || p.ClientIp == filter.ClientIp)
                 && (filter.Type.HasValue || p.Type == filter.Type));
            if (filter.Time.HasValue)
            {
                var endDate = DateTime.UtcNow;
                var startDate = endDate.AddSeconds(filter.Time.Value * -1);
                query = query.Where(t => t.CreatedDate >= startDate);
            }
            var data = query.ProjectTo<UserLogsModel>().ToList();
            return data;
        }

        public async Task Create(UserLogsModel model)
        {
            try
            {
                var data = Mapper.Map<UserLog>(model);
                data.CreatedDate = DateTime.UtcNow;
                _userRepository.Add(data);
                await _unitOfWork.CommitChangesAsync();

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

    }
}
