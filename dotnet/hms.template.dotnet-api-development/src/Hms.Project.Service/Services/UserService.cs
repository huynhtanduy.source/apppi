using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Hms.Project.Common;
using Hms.Project.Common.Extensions;
using Hms.Project.Data;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Common;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Hms.Project.Service.Services
{
    public class UserService : IUserService
    {
        private const string CUSTOM_CLAIM_TYPE = "Permission";
        private const string FUll_NAME = "fullname";
        private const string USER_NAME = "username";
        private const string CREATED = "createddate";

        private readonly AppSettings _appSettings;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly DataContext _dataContext;
        private readonly RoleManager<Role> _roleManager;
        private readonly ISendOTPService _sendOtpService;

        public UserService(IOptions<AppSettings> appSettings, SignInManager<User> signInManager,
            UserManager<User> userManager,
            DataContext dataContext,
            RoleManager<Role> roleManager,
            ISendOTPService sendOtpService
            )
        {
            _appSettings = appSettings.Value;
            _signInManager = signInManager;
            _userManager = userManager;
            _dataContext = dataContext;
            _roleManager = roleManager;
            _sendOtpService = sendOtpService;
        }

        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="createdBy"></param>
        /// <returns></returns>
        /// <exception cref="HmsException"></exception>
        public async Task Create(UserRequestModel userModel,
            Guid createdBy)
        {
            if (_dataContext.Users.Any(u => u.UserName == userModel.User.UserName))
            {
                throw new HmsException(UserIssue.UserExisted);
            }

            userModel.User.CreatedBy = createdBy;
            userModel.User.CreatedDate = DateTime.UtcNow;
            userModel.User.IsDeleted = false;
            userModel.User.EmailConfirmed = true;
            userModel.User.Email = userModel.User.UserName;

            using (var transaction = await _dataContext.Database.BeginTransactionAsync())
            {
                try
                {
                    var result = await _userManager.CreateAsync(userModel.User, userModel.Password);

                    if (result.Succeeded)
                    {
                        var roleNames = await _dataContext.Roles.Where(p => p.Id == userModel.RoleId)
                            .Select(p => p.Name)
                            .ToListAsync();

                        if (!roleNames.Any())
                        {
                            throw new HmsException(UserIssue.UserRoleIdsNotExist);
                        }

                        await _userManager.AddToRolesAsync(userModel.User, roleNames);

                        transaction.Commit();
                    }
                    else
                    {
                        var error = result.Errors.First();
                        throw new HmsException(error.Description, error.Code);
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="updatedBy"></param>
        /// <returns></returns>
        /// <exception cref="HmsException"></exception>
        /// <exception cref="Exception"></exception>
        public async Task Update(UserRequestModel userModel, Guid updatedBy)
        {
            var user = await _userManager.Users
                .Include(p => p.UserRoles)
                .FirstOrDefaultAsync(p => !p.IsDeleted && p.Id == userModel.User.Id);

            if (user == null)
            {
                throw new HmsException(UserIssue.UserNotFound);
            }

            if (userModel.User.UserName != user.UserName &&
                _dataContext.Users.Any(x => x.UserName == userModel.User.UserName))
            {
                throw new HmsException(UserIssue.UserExisted);
            }

            user.FirstName = userModel.User.FirstName;
            user.LastName = userModel.User.LastName;
            user.UserName = userModel.User.UserName;
            user.Avatar = userModel.User.Avatar;
            user.PhoneNumber = userModel.User.PhoneNumber;
            user.Email = userModel.User.UserName;
            user.Deactivated = userModel.User.Deactivated;

            if (!userModel.IsLocked)
            {
                user.LockoutEnd = null;
            }
            if (!userModel.User.Deactivated)
            {
                user.AccessFailedCount = 0;
            }

            using (var transaction = await _dataContext.Database.BeginTransactionAsync())
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(userModel.Password))
                    {
                        user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, userModel.Password);
                        RemoveToken(user.Id);
                    }
                    user.ModifiedDate = DateTime.UtcNow;
                    user.ModifiedBy = updatedBy;

                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        var role = await _roleManager.FindByIdAsync(userModel.RoleId.ToString());
                        if (!await _userManager.IsInRoleAsync(user, role.Name))
                        {
                            result = await _userManager.RemoveFromRolesAsync(user, await _userManager.GetRolesAsync(user));
                            if (result.Succeeded)
                            {
                                await _userManager.AddToRoleAsync(user, role.Name);
                            }
                            else
                            {
                                var error = result.Errors.First();
                                throw new HmsException(error.Description, error.Code);
                            }
                        }
                    }
                    else
                    {
                        var error = result.Errors.First();
                        throw new HmsException(error.Description, error.Code);
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Update Profile
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public async Task UpdateProfile(ProfileRequestModel userModel)
        {
            var user = await _userManager.Users
                .FirstOrDefaultAsync(p => !p.IsDeleted && p.Id == userModel.User.Id);

            if (user == null)
            {
                throw new HmsException(UserIssue.UserNotFound);
            }

            if (userModel.User.UserName != user.UserName &&
                _dataContext.Users.Any(x => x.UserName == userModel.User.UserName))
            {
                throw new HmsException(UserIssue.UserExisted);
            }

            user.FirstName = userModel.User.FirstName;
            user.LastName = userModel.User.LastName;
            user.UserName = userModel.User.UserName;
            user.Avatar = userModel.User.Avatar;
            user.PhoneNumber = userModel.User.PhoneNumber;
            user.Email = userModel.User.UserName;

            using (var transaction = await _dataContext.Database.BeginTransactionAsync())
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(userModel.Password) && !string.IsNullOrWhiteSpace(userModel.OldPassword))
                    {
                        var isCorrectPassword = _userManager.CheckPasswordAsync(user, userModel.OldPassword).Result;
                        if (!isCorrectPassword)
                        {
                            throw new HmsException(UserIssue.WrongOldPassword);
                        }

                        var result = _userManager.ChangePasswordAsync(user, userModel.OldPassword, userModel.Password).Result;
                        if (result.Succeeded)
                        {
                            RemoveToken(user.Id);
                        }
                        else
                        {
                            var error = result.Errors.First();
                            throw new HmsException(error.Description, error.Code);
                        }
                    }

                    user.ModifiedDate = DateTime.UtcNow;
                    user.ModifiedBy = userModel.User.Id;
                    await _userManager.UpdateAsync(user);

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deletedBy"></param>
        /// <returns></returns>
        /// <exception cref="HmsException"></exception>
        public async Task Delete(Guid id, Guid deletedBy)
        {
            if (id == deletedBy)
            {
                throw new HmsException(UserIssue.UserSelfDelete);
            }

            var user = await _dataContext.Users.Where(p => !p.IsDeleted && p.Id == id)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new HmsException(UserIssue.UserNotFound);
            }

            user.IsDeleted = true;
            user.DeletedDate = DateTime.UtcNow;
            user.DeletedBy = deletedBy;

            _dataContext.Users.Update(user);
            await _dataContext.SaveChangesAsync();
        }

        /// <summary>
        /// Get Authenticated
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tokenSession"></param>
        /// <returns></returns>
        public async Task<User> GetUserSession(Guid id, Guid tokenSession)
        {
            return await _dataContext.Users
                .FirstOrDefaultAsync(p =>
                    !p.IsDeleted
                    && !p.Deactivated
                    && p.Id == id
                    && p.UserLoginTokens.FirstOrDefault(u => u.JsonTokenIdentifier == tokenSession) != null
                );
        }

        /// <summary>
        /// Logout
        /// </summary>
        /// <param name="tokenSession"></param>
        /// <param name="userId"></param>
        /// <param name="clientIp"></param>
        /// <returns></returns>
        /// <exception cref="HmsNotFoundException"></exception>
        public async Task Logout(Guid tokenSession, Guid userId, string clientIp)
        {
            RemoveToken(userId, tokenSession);
            await _dataContext.SaveChangesAsync();
            AddUserLog(userId, clientIp, (int)Enumerations.UserLogType.Logout);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="signInModel"></param>
        /// <returns></returns>
        /// <exception cref="HmsUnauthorizedException"></exception>
        public async Task<TokenModel> Authenticate(SignInModel signInModel)
        {
            var user = await AuthenticateWithCheckValidUser(signInModel.Username, signInModel.Password);
            var tokenSession = Guid.NewGuid();
            var expires = DateTime.UtcNow.AddSeconds(_appSettings.TokenLifeTime);
            var accessToken = GenerateJwtToken(user, tokenSession.ToString(), expires);
            var refreshToken = GenerateRefreshToken();

            _dataContext.UserLoginSessions.Add(new UserLoginSession
            {
                JsonTokenIdentifier = tokenSession,
                UserId = user.Id,
                ExpiredDate = expires,
                RefreshToken = refreshToken,
                RefreshExpiredDate = DateTime.UtcNow.AddSeconds(_appSettings.RefreshTokenLifeTime),
                DeviceId = signInModel.DeviceId
            });

            await _dataContext.SaveChangesAsync();
            AddUserLog(user.Id, signInModel.ClientIp, (int)Enumerations.UserLogType.Login);

            return new TokenModel
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
                Expires = expires,
                UserId = user.Id,
                TokenLifeTime = _appSettings.TokenLifeTime
            };
        }

        public async Task<TokenModel> RefreshToken(string refresh)
        {
            var userSession = await _dataContext.UserLoginSessions.SingleOrDefaultAsync(p =>
                p.RefreshToken == refresh && p.RefreshExpiredDate > DateTime.UtcNow);

            if (userSession == null)
            {
                throw new HmsUnauthorizedException(UserIssue.TokenExpired);
            }

            var user = await _userManager.Users
                .Include(p => p.UserRoles)
                .ThenInclude(p => p.Role)
                .SingleOrDefaultAsync(p =>
                    !p.IsDeleted && !p.Deactivated && p.Id == userSession.UserId);

            if (user == null)
            {
                throw new HmsUnauthorizedException(UserIssue.UserNotFound);
            }

            using (var transaction = await _dataContext.Database.BeginTransactionAsync())
            {
                try
                {
                    _dataContext.UserLoginSessions.Remove(userSession);

                    var tokenSession = Guid.NewGuid();
                    var expires = DateTime.UtcNow.AddSeconds(_appSettings.TokenLifeTime);
                    var accessToken = GenerateJwtToken(user, tokenSession.ToString(), expires);
                    var refreshToken = GenerateRefreshToken();

                    _dataContext.UserLoginSessions.Add(new UserLoginSession
                    {
                        JsonTokenIdentifier = tokenSession,
                        UserId = user.Id,
                        ExpiredDate = expires,
                        RefreshToken = refreshToken,
                        RefreshExpiredDate = DateTime.UtcNow.AddSeconds(_appSettings.RefreshTokenLifeTime)
                    });

                    await _dataContext.SaveChangesAsync();
                    transaction.Commit();
                    return new TokenModel
                    {
                        AccessToken = accessToken,
                        RefreshToken = refreshToken,
                        Expires = expires,
                        UserId = user.Id
                    };
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Get one
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isProfile"></param>
        /// <returns></returns>
        public async Task<UserModel> GetOne(Guid id, bool isProfile = false)
        {
            var user = await _userManager.Users
                .Include(p => p.UserRoles)
                .SingleOrDefaultAsync(p => p.Id.Equals(id));
            if (user == null)
            {
                throw new HmsNotFoundException(UserIssue.UserNotFound);
            }

            var userResponse = new UserModel();
            if (isProfile)
            {
                userResponse = new UserModel
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    Username = user.UserName,
                    Email = user.Email,
                    Avatar = user.Avatar,
                    RoleName = user.UserRoles.FirstOrDefault()?.Role.Name,
                    Deactivated = user.Deactivated
                };

                var role = _roleManager.FindByNameAsync(userResponse.RoleName).Result;
                var claims = _roleManager.GetClaimsAsync(role).Result
                    .Where(c => c.Type == CUSTOM_CLAIM_TYPE)
                    .Select(p => p.Value)
                    .ToList();
                userResponse.Permissions = claims;
            }
            else
            {
                userResponse = new UserModel
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    Username = user.UserName,
                    Email = user.Email,
                    Avatar = user.Avatar,
                    RoleId = user.UserRoles.Select(x => x.RoleId).FirstOrDefault(),
                    Deactivated = user.Deactivated,
                    Creator = GetNameById(user.CreatedBy).Result,
                    CreatedDate = user.CreatedDate,
                    LatestModifier = GetNameById(user.ModifiedBy.GetValueOrDefault()).Result,
                    ModifiedDate = user.ModifiedDate,
                    IsLocked = (user.LockoutEnd != null && user.LockoutEnd > DateTime.UtcNow)
                };
            }

            return userResponse;
        }

        /// <summary>
        /// Get List
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PagingResult<UserModel>> GetList(UserFilterQuery query)
        {
            var userQuery = GetUserFilterQuery(query);
            var offset = (query.PageIndex - 1) * query.PageSize;
            var limit = query.PageSize;
            var total = await userQuery.CountAsync();
            var utcNow = DateTime.UtcNow;

            var userResponse = await userQuery
                .Select(p => new UserModel
                {
                    Id = p.Id,
                    FullName = p.FullName,
                    PhoneNumber = p.PhoneNumber,
                    Username = p.UserName,
                    Email = p.Email,
                    RoleName = p.UserRoles.FirstOrDefault().Role.Name,
                    Deactivated = p.Deactivated,
                    IsLocked = (p.LockoutEnd != null && p.LockoutEnd > utcNow)
                })
                .Skip(offset).Take(limit).ToListAsync();

            return new PagingResult<UserModel>(userResponse, total, query.PageIndex, query.PageSize);
        }

        public async Task<string> GetNameById(Guid id)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id.Equals(id));
            return user?.FullName ?? string.Empty;
        }

        public async Task<Guid> AuthenticateWithTwoFactorAuthenticator(SignInWithTwoFactorModel signInWithTwoFactorModel)
        {
            var verifyOtpId = Guid.Empty;
            
            var user = await AuthenticateWithCheckValidUser(signInWithTwoFactorModel.Username, signInWithTwoFactorModel.Password);
            if ((_appSettings.OTP.SendByEmail && !user.Email.IsValid()) || (!_appSettings.OTP.SendByEmail && !user.PhoneNumber.IsValid()))
                throw new HmsException(UserIssue.InvalidSendOTP);

            // Create OTP
            var verifyCode = String.Empty;
            var objVerifyCode = await GetOTPCode(user.Id);
            if (objVerifyCode != null)
            {
                verifyOtpId = objVerifyCode.VerifyCodeId;
                verifyCode = objVerifyCode.VerifyCode;
            }

            // Send OTP by email/ SMS
            if (_appSettings.OTP.SendByEmail)
            {
                _sendOtpService.SendOTPByEmail(user.Email, verifyCode);
            }
            else
            {
                _sendOtpService.SendOTPBySMS(user.PhoneNumber, verifyCode);
            }
            
            return verifyOtpId;
        }
        
        public async Task<TokenModel> VerifyOTPCode(VerifyOTPModel verifyOtpModel)
        {
            var currentDateTime = DateTime.UtcNow;
            var verifyInfo = await _dataContext.VerifyOTPs.FirstOrDefaultAsync(x => x.Id.Equals(verifyOtpModel.VerifyCodeId) &&
                                                                               x.VerifyCode.ToLower().Equals(verifyOtpModel.VerifyCode.ToLower()) &&
                                                                               x.ExpiredDate >= currentDateTime &&
                                                                               !x.IsVerified);
            
            if (verifyInfo == null)
                throw new HmsException(UserIssue.InvalidOTPCode);
            
            var user = await GetUserByIdWithCheckValid(verifyInfo.UserId);
            var tokenSession = Guid.NewGuid();
            var expires = DateTime.UtcNow.AddSeconds(_appSettings.TokenLifeTime);
            var accessToken = GenerateJwtToken(user, tokenSession.ToString(), expires);
            var refreshToken = GenerateRefreshToken();
            await UpdateVerifyOTPCodeStatus(verifyInfo.Id);

            _dataContext.UserLoginSessions.Add(new UserLoginSession
            {
                JsonTokenIdentifier = tokenSession,
                UserId = user.Id,
                ExpiredDate = expires,
                RefreshToken = refreshToken,
                RefreshExpiredDate = DateTime.UtcNow.AddSeconds(_appSettings.RefreshTokenLifeTime),
                DeviceId = verifyOtpModel.DeviceId
            });

            await _dataContext.SaveChangesAsync();
            AddUserLog(user.Id, verifyOtpModel.ClientIp, (int)Enumerations.UserLogType.Login);

            return new TokenModel
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
                Expires = expires,
                UserId = user.Id,
                TokenLifeTime = _appSettings.TokenLifeTime
            };
        }

        #region Private

        private string GenerateJwtToken(User user, string tokenSession, DateTime expires)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.JWT.Secret);
            var subject = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, user.UserRoles.First().Role.Name),
                new Claim(JwtRegisteredClaimNames.Jti, tokenSession),
            });
            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = subject,
                Expires = expires,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature),
                Issuer = _appSettings.JWT.Issuer,
                Audience = _appSettings.JWT.Issuer
            };

            var token = tokenHandler.CreateToken(tokenDescription);

            return tokenHandler.WriteToken(token);
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private IQueryable<User> GetUserFilterQuery(UserFilterQuery query)
        {
            var userQuery = _userManager.Users
                .Where(p =>
                    !p.IsDeleted
                    && (string.IsNullOrEmpty(query.FullName) || p.FirstName.Contains(query.FullName.Trim()) || p.LastName.Contains(query.FullName.Trim()))
                    && (string.IsNullOrEmpty(query.UserName) || p.UserName.Contains(query.UserName.Trim()))
                    && (string.IsNullOrEmpty(query.PhoneNumber) || p.PhoneNumber.Contains(query.PhoneNumber.Trim()))
                    && (string.IsNullOrEmpty(query.SearchTerm)
                        || p.FirstName.Contains(query.SearchTerm.Trim())
                        || p.LastName.Contains(query.SearchTerm.Trim())
                        || p.UserName.Contains(query.SearchTerm.Trim())
                        || p.PhoneNumber.Contains(query.SearchTerm.Trim())
                        || p.UserRoles.FirstOrDefault().Role.Name.Contains(query.SearchTerm.Trim()))
                );

            switch (query.OrderBy.ToLower())
            {
                case FUll_NAME:
                    userQuery = !query.OrderByDesc
                        ? userQuery.OrderBy(p => p.FirstName)
                        : userQuery.OrderByDescending(p => p.FirstName);
                    break;
                case USER_NAME:
                    userQuery = !query.OrderByDesc
                        ? userQuery.OrderBy(p => p.UserName)
                        : userQuery.OrderByDescending(p => p.UserName);
                    break;
                case CREATED:
                    userQuery = !query.OrderByDesc
                        ? userQuery.OrderBy(p => p.CreatedDate)
                        : userQuery.OrderByDescending(p => p.CreatedDate);
                    break;
                default:
                    userQuery = userQuery.OrderByDescending(p => p.CreatedDate);
                    break;
            }

            return userQuery;
        }

        private void RemoveToken(Guid userId, Guid? jti = null)
        {
            var userToken = _dataContext.UserLoginSessions
                .Where(p => p.UserId == userId && (jti == null || p.JsonTokenIdentifier == jti));

            _dataContext.UserLoginSessions.RemoveRange(userToken);
        }

        private void AddUserLog(Guid userId, string clientIp, int logType)
        {
            try
            {
                _dataContext.UserLogs.Add(new UserLog
                {
                    UserId = userId,
                    ClientIp = clientIp,
                    Type = logType
                });
                _dataContext.SaveChanges();
            }
            catch (Exception) { }
        }
        
        private async Task UpdateVerifyOTPCodeStatus(Guid id)
        {
            var verifyOTPQuery = await _dataContext.VerifyOTPs.FirstOrDefaultAsync(x => x.Id.Equals(id));
            if (verifyOTPQuery?.Id.IsValid() == true)
            {
                verifyOTPQuery.IsVerified = true;
                await _dataContext.SaveChangesAsync(true);
            }
        }

        private async Task<User> GetUserByIdWithCheckValid(Guid userId)
        {
            var user = new User();
            
            if (userId.IsValid())
            {
                user = await _userManager.Users
                    .Include(p => p.UserRoles)
                    .ThenInclude(p => p.Role)
                    .SingleOrDefaultAsync(p =>
                        !p.IsDeleted && !p.Deactivated && p.Id.Equals(userId));

                if (user == null)
                {
                    throw new HmsException(UserIssue.UserNotFound);
                }

                if (!user.EmailConfirmed)
                {
                    throw new HmsException(UserIssue.EmailNotConfirmed);
                }
            }

            return user;
        }

        private async Task<User> AuthenticateWithCheckValidUser(string userName, string password)
        {
            var user = new User();
            
            var loginResult = await _signInManager.PasswordSignInAsync(userName, password, false, true);

            if (loginResult.IsLockedOut)
                throw new HmsException(UserIssue.UserLocked);

            if (!loginResult.Succeeded)
                throw new HmsException(UserIssue.UserLockWarning);

            user = await _userManager.Users
                .Include(p => p.UserRoles)
                .ThenInclude(p => p.Role)
                .SingleOrDefaultAsync(p =>
                    !p.IsDeleted && !p.Deactivated && p.NormalizedUserName == userName.ToUpper());

            if (user == null)
            {
                throw new HmsException(UserIssue.UserNotFound);
            }

            if (!user.EmailConfirmed)
            {
                throw new HmsException(UserIssue.EmailNotConfirmed);
            }
            
            return user;
        }

        private string GenerateOTPCode()
        {
            string newCharacters = "";
 
            //This one tells you which characters are allowed in this new password
            string allowedChars = "";
            //Here Specify your OTP Characters
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            //If you Need more secure OTP then uncomment Below Lines 
            //  allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";        
            // allowedChars += "~,!,@,#,$,%,^,&,*,+,?";
            
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
 
            string IDString = "";
            string temp = "";
 
            //utilize the "random" class
            Random rand = new Random();
 
        
            for (int i = 0; i < Convert.ToInt32(_appSettings.OTP.CodeSize); i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                IDString += temp;
                newCharacters = IDString;
            }
 
            return newCharacters;
        }

        private async Task<VerifyOTPModel> GetOTPCode(Guid userId)
        {
            var result = new VerifyOTPModel();
            try
            {
                var currentDateTime = DateTime.UtcNow;
                var verifyInfo = await _dataContext.VerifyOTPs.FirstOrDefaultAsync(x => x.ExpiredDate > currentDateTime &&
                                                                                        !x.IsVerified && x.UserId.Equals(userId));
                if (verifyInfo == null)
                {
                    var verifyCode = GenerateOTPCode();
                    var verifyOtpData = new VerifyOTP
                    {
                        UserId = userId,
                        VerifyCode = verifyCode,
                        ExpiredDate = DateTime.UtcNow.AddMinutes(_appSettings.OTP.ExpireMinutes)
                    };

                    await _dataContext.VerifyOTPs.AddAsync(verifyOtpData);
                    await _dataContext.SaveChangesAsync();
                    result.VerifyCodeId = verifyOtpData.Id;
                    result.VerifyCode = verifyCode;
                }
                else
                {
                    verifyInfo.ExpiredDate = DateTime.UtcNow.AddMinutes(_appSettings.OTP.ExpireMinutes);
                    await _dataContext.SaveChangesAsync(true);
                    result.VerifyCodeId = verifyInfo.Id;
                    result.VerifyCode = verifyInfo.VerifyCode;
                }
            } catch (Exception) {}

            return result;
        }

        #endregion
    }
}