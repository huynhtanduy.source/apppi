﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hms.Project.Service.Interfaces.AzureUploadFile;
using Hms.Project.Service.Models.AzureUploadFile;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.FileProviders;

namespace Hms.Project.Service.Services.AzureUploadFile
{
    public class AzureBlobStorageService : IAzureBlobStorageService
    {
        private const int CHUNK_LIMIT = 1024 * 1024;

        private static Dictionary<String, SessionFile> SessionFiles;

        private static PhysicalFileProvider _fileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());

        #region " Public "

        public AzureBlobStorageService(AzureBlobSetings settings)
        {
            this.settings = settings;
        }

        public SessionFile CreateSessionFile(String fileName, int chunkSize, long fileSize)
        {

            if (String.IsNullOrWhiteSpace(fileName))
                throw new Exception("File name missing");



            if (chunkSize > CHUNK_LIMIT)
                throw new Exception(String.Format("Maximum chunk size is {0} bytes", CHUNK_LIMIT));

            if (chunkSize < 1)
                throw new Exception("Chunk size must be greater than zero");

            if (fileSize < 1)
                throw new Exception("Total size must be greater than zero");
            if (SessionFiles == null)
            {
                SessionFiles = new Dictionary<String, SessionFile>();
            }
            SessionFile SessionFile = new SessionFile(new FileInformation(fileSize, fileName, chunkSize));
            SessionFiles.Add(SessionFile.Id, SessionFile);


            return SessionFile;
        }

        public SessionFile GetSessionFile(String id)
        {
            return SessionFiles[id];
        }

        public List<SessionFile> GetAllSessionFiles()
        {
            return SessionFiles.Values.ToList();
        }


        public async Task<string> UploadBlockAsync(string fileId, string blockId, Stream data)
        {
            CloudBlobContainer container = await GetContainerAsync();
            container.CreateIfNotExists();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileId);
            await blockBlob.PutBlockAsync(blockId, data, null);
            return blockBlob.Uri.OriginalString;
        }

        public async Task<string> CommitBlockAsync(string fileId, List<string> blockIds)
        {
            CloudBlobContainer container = await GetContainerAsync();
            container.CreateIfNotExists();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileId);
            await blockBlob.PutBlockListAsync(blockIds);
            return blockBlob.Uri.OriginalString;
        }
        public async Task UploadLagreAsync(string blobName, string folderName)
        {
            //Blob
            CloudBlobContainer container = await GetContainerAsync();
            container.CreateIfNotExists();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(folderName);
            string path = Path.Combine("./files_store", folderName);
            var blockIds = new List<string>();
            foreach (string pathFile in Directory.EnumerateFiles(path, "*.*", SearchOption.AllDirectories))
            {
                var fileInfo = _fileProvider.GetFileInfo(pathFile);
                var blockId = Convert.ToBase64String(Encoding.UTF8.GetBytes(fileInfo.Name));
                await UploadBlockAsync(blobName, blockId, fileInfo.CreateReadStream());
                blockIds.Add(blockId);
            }
            await CommitBlockAsync(blobName, blockIds);

        }
        public async Task UploadAsync(string blobName, string filePath)
        {
            //Blob
            CloudBlockBlob blockBlob = await GetBlockBlobAsync(blobName);

            //Upload
            using (var fileStream = System.IO.File.Open(filePath, FileMode.Open))
            {
                fileStream.Position = 0;
                await blockBlob.UploadFromStreamAsync(fileStream);
            }
        }

        public async Task UploadAsync(string blobName, Stream stream)
        {
            //Blob
            CloudBlockBlob blockBlob = await GetBlockBlobAsync(blobName);

            //Upload
            stream.Position = 0;
            await blockBlob.UploadFromStreamAsync(stream);
        }

        public async Task<MemoryStream> DownloadAsync(string blobName)
        {
            //Blob
            CloudBlockBlob blockBlob = await GetBlockBlobAsync(blobName);

            //Download
            using (var stream = new MemoryStream())
            {
                await blockBlob.DownloadToStreamAsync(stream);
                return stream;
            }
        }

        public async Task DownloadAsync(string blobName, string path)
        {
            //Blob
            CloudBlockBlob blockBlob = await GetBlockBlobAsync(blobName);

            //Download
            await blockBlob.DownloadToFileAsync(path, FileMode.Create);
        }

        public async Task DeleteAsync(string blobName)
        {
            //Blob
            CloudBlockBlob blockBlob = await GetBlockBlobAsync(blobName);

            //Delete
            await blockBlob.DeleteAsync();
        }

        public async Task<bool> ExistsAsync(string blobName)
        {
            //Blob
            CloudBlockBlob blockBlob = await GetBlockBlobAsync(blobName);

            //Exists
            return await blockBlob.ExistsAsync();
        }

        public async Task<List<AzureBlobItem>> ListAsync()
        {
            return await GetBlobListAsync();
        }

        public async Task<List<AzureBlobItem>> ListAsync(string rootFolder)
        {
            if (rootFolder == "*") return await ListAsync(); //All Blobs
            if (rootFolder == "/") rootFolder = "";          //Root Blobs

            var list = await GetBlobListAsync();
            return list.Where(i => i.Folder == rootFolder).ToList();
        }

        public async Task<List<string>> ListFoldersAsync()
        {
            var list = await GetBlobListAsync();
            return list.Where(i => !string.IsNullOrEmpty(i.Folder))
                       .Select(i => i.Folder)
                       .Distinct()
                       .OrderBy(i => i)
                       .ToList();
        }

        public async Task<List<string>> ListFoldersAsync(string rootFolder)
        {
            if (rootFolder == "*" || rootFolder == "") return await ListFoldersAsync(); //All Folders

            var list = await GetBlobListAsync();
            return list.Where(i => i.Folder.StartsWith(rootFolder))
                       .Select(i => i.Folder)
                       .Distinct()
                       .OrderBy(i => i)
                       .ToList();
        }

        #endregion

        #region " Private "

        private readonly AzureBlobSetings settings;

        private async Task<CloudBlobContainer> GetContainerAsync()
        {
            //Account
            CloudStorageAccount storageAccount = new CloudStorageAccount(
                new StorageCredentials(settings.StorageAccount, settings.StorageKey), false);

            //Client
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            //Container
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(settings.ContainerName);
            await blobContainer.CreateIfNotExistsAsync();
            //await blobContainer.SetPermissionsAsync(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Blob });

            return blobContainer;
        }

        private async Task<CloudBlockBlob> GetBlockBlobAsync(string blobName)
        {
            //Container
            CloudBlobContainer blobContainer = await GetContainerAsync();

            //Blob
            CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(blobName);

            return blockBlob;
        }

        private async Task<List<AzureBlobItem>> GetBlobListAsync(bool useFlatListing = true)
        {
            //Container
            CloudBlobContainer blobContainer = await GetContainerAsync();

            //List
            var list = new List<AzureBlobItem>();
            BlobContinuationToken token = null;
            do
            {
                BlobResultSegment resultSegment =
                    await blobContainer.ListBlobsSegmentedAsync("", useFlatListing, new BlobListingDetails(), null, token, null, null);
                token = resultSegment.ContinuationToken;

                foreach (IListBlobItem item in resultSegment.Results)
                {
                    list.Add(new AzureBlobItem(item));
                }
            } while (token != null);

            return list.OrderBy(i => i.Folder).ThenBy(i => i.Name).ToList();
        }

        #endregion
    }
}
