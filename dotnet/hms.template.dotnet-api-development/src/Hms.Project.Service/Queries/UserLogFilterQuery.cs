﻿using System;

namespace Hms.Project.Service.Queries
{
    public class UserLogFilterQuery
    {
        public Guid? UserId { get; set; }
        public int? Type { get; set; }
        public string Link { get; set; }

        public long? Time { get; set; }
        public string ClientIp { get; set; }
    }
}
