﻿using System;
using System.Collections.Generic;
using System.Text;
using Hms.Project.Service.Models;

namespace Hms.Project.Service.Queries
{
    public class UserFilterQuery : PagingQuery
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
