using System;

namespace Hms.Project.Service.Queries
{
    public class AuditFilterQuery : PagingQuery
    {
        public Guid Id { get; set; }
        public string TableName { get; set; }
        public string ColumnNames { get; set; }
    }
}