﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hms.Project.Service.Queries
{
    public class RoleFilterQuery : PagingQuery
    {
        public string Name { get; set; }
    }
}
