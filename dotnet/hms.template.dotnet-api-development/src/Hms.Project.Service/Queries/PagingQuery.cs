﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Hms.Project.Service.Queries
{
    public class PagingQuery
    {
        public PagingQuery()
        {
            PageIndex = 1;
            PageSize = 10;
            OrderBy = string.Empty;
            OrderByDesc = false;
        }

        public string SearchTerm { get; set; }
        [Range(1, 100, ErrorMessage = "Page size is positive number only")]
        public int PageSize { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Page Index is positive number only")]
        public int PageIndex { get; set; }
        public string OrderBy { get; set; }
        public bool OrderByDesc { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
