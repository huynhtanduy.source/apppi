﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Hms.Project.Service.Models.AzureUploadFile;

namespace Hms.Project.Service.Interfaces.AzureUploadFile
{
    public interface IAzureBlobStorageService
    {
        Task UploadAsync(string blobName, string filePath);
        Task UploadAsync(string blobName, Stream stream);
        Task UploadLagreAsync(string blobName, string folderName);
        Task<MemoryStream> DownloadAsync(string blobName);
        Task DownloadAsync(string blobName, string path);
        Task DeleteAsync(string blobName);
        Task<bool> ExistsAsync(string blobName);
        Task<List<AzureBlobItem>> ListAsync();
        Task<List<AzureBlobItem>> ListAsync(string rootFolder);
        Task<List<string>> ListFoldersAsync();
        Task<List<string>> ListFoldersAsync(string rootFolder);
        Task<string> UploadBlockAsync(string fileId, string blockId, Stream data);
        SessionFile CreateSessionFile(string fileName, int value1, long value2);
        Task<string> CommitBlockAsync(string fileId, List<string> blockIds);
        SessionFile GetSessionFile(string sessionId);
    }
}
