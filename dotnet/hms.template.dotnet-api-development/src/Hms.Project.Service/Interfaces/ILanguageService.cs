﻿using System.Threading.Tasks;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;

namespace Hms.Project.Service.Interfaces
{
    public interface ILanguageService
    {
        string ReadJsonOject(string language);
        Task<LanguageModel> Update(LanguageModel languageModel);
        Task<PagingResult<LanguageItemModel>> GetAll(LanguageFilterQuery query);
    }
}
