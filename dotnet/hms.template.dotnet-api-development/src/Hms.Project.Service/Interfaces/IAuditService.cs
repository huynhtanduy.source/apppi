using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;

namespace Hms.Project.Service.Interfaces
{
    public interface IAuditService
    {
        Task<PagingResult<GetAuditPagingResultModel>> GetPaging(AuditFilterQuery query);
        Task<IList<GetAuditDetailsResultModel>> GetAuditById(Guid id);
    }
}