using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.IdentityModel.Tokens;

namespace Hms.Project.Service.Interfaces
{
    public interface IRoleService
    {
        Task Create(RoleRequestModel roleRequestModel, Guid userId);
        Task<RoleDetailModel> GetOne(Guid id);
        Task<PagingResult<RoleModel>> GetList(RoleFilterQuery query);
        Task<ICollection<RoleModel>> GetList();
        Task Update(RoleRequestModel roleRequest, Guid userId);
        Task Delete(Guid id);
    }
}