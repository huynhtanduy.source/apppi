﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;

namespace Hms.Project.Service.Interfaces
{
    public interface IUserLogsService
    {
        Task Create(UserLogsModel model);
        Task<List<UserLogsModel>> GetAll(UserLogFilterQuery filter);
    }
}
