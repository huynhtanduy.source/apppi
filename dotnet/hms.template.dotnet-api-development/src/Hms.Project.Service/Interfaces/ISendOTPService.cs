using System.Threading.Tasks;

namespace Hms.Project.Service.Interfaces
{
    public interface ISendOTPService
    {
        bool SendOTPByEmail(string email, string OTP);
        bool SendOTPBySMS(string phoneNumber, string OTP);
    }
}