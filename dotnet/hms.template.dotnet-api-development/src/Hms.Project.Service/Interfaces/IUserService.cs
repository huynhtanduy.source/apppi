
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;

namespace Hms.Project.Service.Interfaces
{
    public interface IUserService
    {
        Task<TokenModel> Authenticate(SignInModel signInModel);
        Task Logout(Guid tokenSession, Guid userId, string clientIp);
        Task<UserModel> GetOne(Guid id, bool isProfile = false);
        Task<PagingResult<UserModel>> GetList(UserFilterQuery query);
        Task<User> GetUserSession(Guid id, Guid tokenSession);
        Task Create(UserRequestModel userModel, Guid createdBy);
        Task Update(UserRequestModel userModel, Guid updatedBy);
        Task Delete(Guid id, Guid deletedBy);
        Task UpdateProfile(ProfileRequestModel userModel);
        Task<TokenModel> RefreshToken(string refresh);
        Task<string> GetNameById(Guid id);
        Task<Guid> AuthenticateWithTwoFactorAuthenticator(SignInWithTwoFactorModel signInModel);
        Task<TokenModel> VerifyOTPCode(VerifyOTPModel verifyOtpModel);
    }
}