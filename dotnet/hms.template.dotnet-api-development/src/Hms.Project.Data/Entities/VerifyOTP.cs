using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hms.Project.Data.Entities
{
    public class VerifyOTP
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public string VerifyCode { get; set; }
        public DateTime ExpiredDate { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
        public bool IsVerified { get; set; } = false;
    }
}