using System;
using Microsoft.AspNetCore.Identity;

namespace Hms.Project.Data.Entities
{
    public class UserRole : IdentityUserRole<Guid>, IEntity
    {
        public virtual User User { get; set; }
        public virtual Role Role { get; set; }
    }
}