using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Hms.Project.Data.Entities
{
    public class User : IdentityUser<Guid>, IEntity
    {
        public string FirstName  { get; set; }
        public string LastName { get; set; }
        public string Avatar { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public Guid CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public Guid? DeletedBy { get; set; }
        public bool IsSystemUser { get; set; }
        public bool Deactivated { get; set; }
        public bool IsDeleted { get; set; }
        public string FullName => $"{FirstName} {LastName}";

        public virtual ICollection<UserLoginSession> UserLoginTokens { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}