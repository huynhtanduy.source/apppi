﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hms.Project.Data.Entities
{
    public class UserLog : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid? UserId { get; set; }
        public string ClientIp { get; set; }
        public string Link { get; set; }
        [Required]
        public int Type { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    }
}
