using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace Hms.Project.Data.Entities
{
    public class Role : IdentityRole<Guid>, IEntity
    {
        [Key, JsonIgnore] 
        public override Guid Id { get; set; } = Guid.NewGuid();
        [Required, JsonIgnore]
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
        [Required, JsonIgnore]
        public Guid CreatedBy { get; set; }
        [JsonIgnore]
        public DateTime? ModifiedDate { get; set; }
        [JsonIgnore]
        public Guid? ModifiedBy { get; set; }
        [JsonIgnore]
        public ICollection<UserRole> UserRoles { get; set; }
        [JsonIgnore]
        public override string NormalizedName { get; set; }
        [JsonIgnore]
        public override string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

    }
}