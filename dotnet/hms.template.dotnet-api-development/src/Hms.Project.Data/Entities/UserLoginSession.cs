using System;

namespace Hms.Project.Data.Entities
{
    public class UserLoginSession : IEntity
    {
            public Guid JsonTokenIdentifier { get; set; }
            public Guid UserId { get; set; }
            public DateTime ExpiredDate { get; set; }
            public string RefreshToken { get; set; }
            public DateTime RefreshExpiredDate { get; set; }
            public string DeviceId { get; set; }
            public User User { get; set; }
    }
}