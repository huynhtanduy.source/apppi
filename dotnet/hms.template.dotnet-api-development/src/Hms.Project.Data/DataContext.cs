using System;
using System.Threading.Tasks;
using Hms.Project.Common.Extensions;
using Hms.Project.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hms.Project.Data
{
    public class DataContext : IdentityDbContext<User, Role, Guid,
        IdentityUserClaim<Guid>,
        UserRole, IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public DbSet<UserLoginSession> UserLoginSessions { get; set; }
        public DbSet<UserLog> UserLogs { get; set; }
        public DbSet<Audit> Audits { get; set; }
        public DbSet<VerifyOTP> VerifyOTPs { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(b => { b.ToTable("Users"); });
            modelBuilder.Entity<Role>(b => { b.ToTable("Roles"); });
            modelBuilder.Entity<UserRole>(b =>
            {
                b.ToTable("UserRoles");
                b.HasKey(ur => new {ur.UserId, ur.RoleId});

                b.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                b.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            modelBuilder.Entity<IdentityUserClaim<Guid>>(b => { b.ToTable("UserClaims"); });
            modelBuilder.Entity<IdentityUserLogin<Guid>>(b => { b.ToTable("UserLogins"); });
            modelBuilder.Entity<IdentityUserToken<Guid>>(b => { b.ToTable("UserTokens"); });
            modelBuilder.Entity<IdentityRoleClaim<Guid>>(b => { b.ToTable("RoleClaims"); });

            modelBuilder.Entity<UserLoginSession>(ConfigureUserLoginSession);
            modelBuilder.Entity<UserLog>(ConfigureUserLogs);
            modelBuilder.Entity<Audit>(ConfigureAudits);
            modelBuilder.Entity<VerifyOTP>(ConfigureVerifyOTPs);
        }

        private static void ConfigureUserLoginSession(EntityTypeBuilder<UserLoginSession> builder)
        {
            builder.HasKey(u => new {u.UserId, TokenSession = u.JsonTokenIdentifier});
            builder.HasOne(ut => ut.User).WithMany(u => u.UserLoginTokens).HasForeignKey(ut => ut.UserId);
        }

        private static void ConfigureUserLogs(EntityTypeBuilder<UserLog> entity)
        {
            entity.HasIndex(x => new {x.UserId, x.CreatedDate});
        }

        private static void ConfigureAudits(EntityTypeBuilder<Audit> entity)
        {
            entity.HasIndex(x => new {x.TableName, x.Action, x.KeyFieldId, x.AuditDate});
            entity.HasIndex(x => new {x.TableName, x.Action, x.AuditDate});
        }

        private static void ConfigureVerifyOTPs(EntityTypeBuilder<VerifyOTP> entity)
        {
            entity.HasIndex(x => new {x.UserId, x.VerifyCode, x.ExpiredDate});
            entity.HasIndex(x => new {x.UserId, x.VerifyCode});
        }

        #region Save tracking

        public void SaveAuditTrackingLog(Audit data)
        {
            try
            {
                if (data.UserId.IsValid() && data.KeyFieldId.IsValid() && !string.Equals(data.OldValues, data.NewValues))
                {
                    Task.Run(() => Audits.Add(data));
                }
            }
            catch { }
        }

        #endregion Save tracking
    }
}