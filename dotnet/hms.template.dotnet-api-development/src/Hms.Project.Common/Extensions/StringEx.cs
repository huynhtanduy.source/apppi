using System.Linq;
using Newtonsoft.Json;

namespace Hms.Project.Common.Extensions
{
    public static class StringEx
    {
        public static bool IsValid(this string val)
        {
            return !string.IsNullOrWhiteSpace(val);
        }
        
        public static string ToJson(this object obj) {
            if (obj == null) return string.Empty;
            return JsonConvert.SerializeObject(obj);
        }
    }
}