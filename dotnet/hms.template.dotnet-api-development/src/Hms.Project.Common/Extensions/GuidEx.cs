using System;

namespace Hms.Project.Common.Extensions
{
    public static class GuidEx
    {
        public static bool IsValid(this Guid val)
        {
            return val != Guid.Empty;
        }
    }
}