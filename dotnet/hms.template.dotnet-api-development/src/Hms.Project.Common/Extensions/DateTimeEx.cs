using System;

namespace Hms.Project.Common.Extensions
{
    public static class DateTimeEx
    {
        public static DateTime? IsMinDate(this DateTime d, bool toUtc = true)
        {
            if (d.Date.Equals(DateTime.MinValue.Date))
                return null;
            else
                return toUtc ? d.ToUniversalTime() : d;
        }
    }
}