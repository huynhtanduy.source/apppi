using System;

namespace Hms.Project.Common
{
    public class HmsUnauthorizedException : HmsException
    {
        public HmsUnauthorizedException(string message) : base(message)
        {
        }

        public HmsUnauthorizedException(string message, int code) : base(message)
        {
            Data.Add(ErrorCode, code);
        }

        public HmsUnauthorizedException(Enum exceptionEnum, string exValue = "") : base($"{exceptionEnum.GetDescription()} {exValue}")
        {
            Data.Add(ErrorCode, exceptionEnum.GetHashCode());
        }
    }
}