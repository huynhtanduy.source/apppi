using System;
using System.Globalization;

namespace Hms.Project.Common
{
    public class HmsException : Exception
    {
        public const string ErrorCode = "error_code";
        public HmsException(string message) : base(message)
        {
        }

        public HmsException(string message, params object[] args) : base(string.Format(CultureInfo.CurrentCulture,
            message, args))
        {
        }

        public HmsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public HmsException(string message, int code) : base(message)
        {
            Data.Add(ErrorCode, code);
        }

        public HmsException(Enum exceptionEnum, string exValue = "") : base($"{exceptionEnum.GetDescription()} {exValue}")
        {
            Data.Add(ErrorCode, exceptionEnum.GetHashCode());
        }
    }
}