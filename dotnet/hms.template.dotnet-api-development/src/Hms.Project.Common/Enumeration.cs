﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hms.Project.Common
{
    public class Enumeration
    {
        public static class RegularExpression
        {
            public const string Password = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W]).{6,}$";
            public const string PhoneNumber = @"^[+0-9]{1}[0-9]+$";
        }
    }
}
