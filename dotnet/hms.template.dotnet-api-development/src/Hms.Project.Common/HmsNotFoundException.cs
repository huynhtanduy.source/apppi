using System;

namespace Hms.Project.Common
{
    public class HmsNotFoundException : HmsException
    {
        public HmsNotFoundException(string message, int code) : base(message, code)
        {
        }

        public HmsNotFoundException(string message) : base(message)
        {
        }

        public HmsNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public HmsNotFoundException(Enum exceptionEnum, string exValue = "") : base($"{exceptionEnum.GetDescription()} {exValue}")
        {
            Data.Add(ErrorCode, exceptionEnum.GetHashCode());
        }
    }
}