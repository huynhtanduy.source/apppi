﻿namespace Hms.Project.Api.Models.Requests
{
    public class LanguageRequest
    {
        public string Id { get; set; }
        public string Value { get; set; }
        public string Language { get; set; }
    }

}
