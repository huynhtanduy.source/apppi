using System;
using System.ComponentModel.DataAnnotations;
using Hms.Project.Common;

namespace Hms.Project.Api.Models.Requests
{
    public class SignInRequest
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string GoogleCaptchaResponse { get; set; }
        public string DeviceId { get; set; }
    }

    public class UserRequest
    {
        [Required]
        [EmailAddress(ErrorMessage = "Wrong email format")]
        public string Username { get; set; }
        [RegularExpression(Enumeration.RegularExpression.Password, ErrorMessage = "Does not meet password requirements.")]
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [RegularExpression(Enumeration.RegularExpression.PhoneNumber, ErrorMessage = "Wrong phone number format")]
        public string PhoneNumber { get; set; }
        public string Avatar { get; set; }
    }

    public class UpdateUserRequest : UserRequest
    {
        [Required]
        public Guid RoleId { get; set; }
        public bool IsLocked { get; set; }
        public bool Deactivated { get; set; }
    }

    public class UpdateProfileRequest : UserRequest
    {
        public string OldPassword { get; set; }
    }

    public class RefreshTokenRequest
    {
        [Required]
        public string RefreshToken { get; set; }
    }
    
    public class SignInWithTwoFactorRequest
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string GoogleCaptchaResponse { get; set; }
    }
}