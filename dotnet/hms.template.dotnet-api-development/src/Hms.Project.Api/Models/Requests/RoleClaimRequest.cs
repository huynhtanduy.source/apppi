using System;
using System.Collections.Generic;

namespace Hms.Project.Api.Models.Requests
{
    public class RoleClaimRequest
    {
        public Guid RoleId { get; set; }
        public HashSet<ClaimRequest> Claims { get; set; }

        public RoleClaimRequest() => Claims = new HashSet<ClaimRequest>();
    }
}