﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hms.Project.Api.Models.Requests
{
    public class RoleRequest
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public List<string> Permissions { get; set; }
    }
}