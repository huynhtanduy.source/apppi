﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hms.Project.Api.Models.Requests.UploadFile
{
    public class CreateSessionParamsRequest
    {
        [FromForm]
        public long UserId { get; set; }
        /// <summary>
        /// Size of data block (in bytes)
        /// </summary>
        [FromForm]
        public int? ChunkSize { get; set; }

        /// <summary>
        /// Total file size (in bytes)
        /// </summary>
        [FromForm]
        public long? TotalSize { get; set; }

        /// <summary>
        /// File name
        /// </summary>
        [FromForm]
        public string FileName { get; set; }
    }
}
