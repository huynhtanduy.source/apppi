﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hms.Project.Api.Models.Requests.UploadFile
{
    public class UploadFileAzureRequest
    {
        public string SessionId { get; set; }
    }
    public class UploadFileChunkRequest
    {
        public long? UserId { get; set; }
        [FromRoute]
        public string SessionId { get; set; }
        [FromQuery]
        public int? ChunkNumber { get; set; }
        [FromForm]
        public IFormFile InputFile { get; set; }
    }
}
