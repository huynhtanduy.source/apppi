namespace Hms.Project.Api.Models.Requests
{
    public class ClaimRequest
    {
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
    }
}