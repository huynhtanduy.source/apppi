using System;
using System.ComponentModel.DataAnnotations;

namespace Hms.Project.Api.Models.Requests
{
    public class VerifyOTPRequest
    {
        [Required]
        public Guid VerifyCodeId { get; set; }
        [Required]
        public string VerifyCode { get; set; }
        public string DeviceId { get; set; }
    }
}