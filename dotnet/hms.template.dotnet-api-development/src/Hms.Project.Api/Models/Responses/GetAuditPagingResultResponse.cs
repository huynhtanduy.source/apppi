using System;

namespace Hms.Project.Api.Models.Responses
{
    public class GetAuditPagingResultResponse : BaseResponse
    {
        public Guid Id { get; set; }
        public string Action { get; set; }
        public string ColumnNames { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}