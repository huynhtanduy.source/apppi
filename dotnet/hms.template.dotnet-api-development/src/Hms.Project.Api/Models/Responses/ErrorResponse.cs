using System.Collections.Generic;
using Newtonsoft.Json;

namespace Hms.Project.Api.Models.Responses
{
    public class ErrorResponse
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<ErrorItem> Errors { get; set; }

        public ErrorResponse(ErrorItem errorItem)
            : this()
        {
            Errors.Add(errorItem);
        }

        public ErrorResponse(ICollection<ErrorItem> errors)
            : this()
        {
            Errors = errors;
        }

        public ErrorResponse(string message) : this()
        {
            Errors.Add(new ErrorItem
            {
                Message = message,
            });
        }

        public ErrorResponse(int code, string message)
            : this()
        {
            Errors.Add(new ErrorItem {
                    Code = code,
                    Message = message,
            });
        }

        public ErrorResponse()
        {
            Errors = new List<ErrorItem>();
        }
    }

    public class ErrorItem
    {
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int Code { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Message { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public object MoreInfo { get; set; }
    }
}