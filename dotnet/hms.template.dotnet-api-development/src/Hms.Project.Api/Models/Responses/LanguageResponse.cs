﻿namespace Hms.Project.Api.Models.Responses
{
    public class LanguageResponse
    {
        public string Language { get; set; }
        public string Value { get; set; }
        public string LastUpdate { get; set; }
    }
    public class LanguageItemResponse
    {
        public string Id { get; set; }
        public string ValueEN { get; set; }
        public string ValueVN { get; set; }
    }
}
