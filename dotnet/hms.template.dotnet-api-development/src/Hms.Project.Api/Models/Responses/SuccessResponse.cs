using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Hms.Project.Api.Models.Responses
{
    public class SuccessResponse<T>
    {
        public SuccessResponse()
        {
        }

        [DataMember]
        public T Data { get; set; }

        public SuccessResponse(T data)
        {
            Data = data;
        }
    }
}