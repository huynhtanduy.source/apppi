﻿
using System;
using Hms.Project.Service.Models.AzureUploadFile;

namespace Hms.Project.Api.Models.Responses.UploadFile
{
    public class SessionCreationStatusResponse
    {
        public SessionCreationStatusResponse() { }
        public static SessionCreationStatusResponse FromSession(SessionFile session)
        {
            return new SessionCreationStatusResponse
            {

                SessionId = session.Id,
                FileName = session.FileInfo.FileName
            };
        }
        public String FileName { get; set; }

        public String SessionId { get; set; }

        public long UserId { get; set; }
    }
}
