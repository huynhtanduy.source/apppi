namespace Hms.Project.Api.Models.Responses
{
    public class GetAuditDetailsResultResponse
    {
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string TableName { get; set; }
    }
}