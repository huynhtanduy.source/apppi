﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hms.Project.Api.Models.Responses
{
    public class RoleResponse : BaseResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class RoleDetailResponse : RoleResponse
    {
        public List<string> Permissions { get; set; }
    }
}