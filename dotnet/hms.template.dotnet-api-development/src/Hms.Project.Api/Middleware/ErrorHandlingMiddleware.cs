using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Hms.Project.Api.Models.Responses;
using Hms.Project.Common;
using Hms.Project.Service.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog.Context;
using Serilog.Events;

namespace Hms.Project.Api.Middleware
{
    public class ErrorHandlingMiddleware
    {

        private readonly RequestDelegate _next;

        private readonly ILogger<ErrorHandlingMiddleware> _logger;

        private readonly AppSettings _appSettings;

        public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _logger = logger;
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var start = Stopwatch.GetTimestamp();
            if (!context.Request.Path.Value.Contains("upload") && _appSettings.IsWriteLog)
            {
                var originalBodyStream = context.Response.Body;
                var bodyRequestText = await GetBodyAsync(context);

                try
                {
                    using (var responseBody = new MemoryStream())
                    {
                        context.Response.Body = responseBody;
                        await _next(context);
                        var bodyResponseText = await FormatResponse(context.Response);
                        await responseBody.CopyToAsync(originalBodyStream);
                        var elapsedMs = GetElapsedMilliseconds(start, Stopwatch.GetTimestamp());
                        var statusCode = context.Response?.StatusCode;
                        var level = statusCode > 499 ? LogEventLevel.Error : LogEventLevel.Information;
                        WriteConsole(context.TraceIdentifier, context.Request.Method, GetPath(context), bodyRequestText, bodyResponseText);
                    }
                }
                catch (Exception ex)
                {
                    await HandleExceptionAsync(context, bodyRequestText, start, originalBodyStream, ex);
                }
            }
            else
            {
                try
                {
                    await _next(context);
                }
                catch (Exception ex)
                {
                    await HandleExceptionAsync(context, String.Empty, start, new MemoryStream(), ex);
                }
            }
        }
        private async Task<string> FormatResponse(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            string text = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);
            return text;
        }
        private async Task HandleExceptionAsync(HttpContext context, string bodyRequestText, long start, Stream originalBodyStream, Exception exception)
        {

            using (var responseBody = new MemoryStream())
            {
                if (_appSettings.IsWriteLog)
                {
                    context.Response.Body = responseBody;
                }
                var elapsedMs = GetElapsedMilliseconds(start, Stopwatch.GetTimestamp());
                var code = HttpStatusCode.InternalServerError; // 500 if unexpected
                var errorMessage = "";
                if (exception is HmsNotFoundException)
                {
                    errorMessage = exception.InnerException != null
                        ? exception.InnerException.Message
                        : exception.Message;
                    code = HttpStatusCode.NotFound;
                }
                else if (exception is HmsUnauthorizedException)
                {
                    errorMessage = exception.InnerException != null
                        ? exception.InnerException.Message
                        : exception.Message;
                    code = HttpStatusCode.Unauthorized;
                }
                else if (exception is HmsException)
                {
                    errorMessage = exception.InnerException != null
                       ? exception.InnerException.Message
                       : exception.Message;
                    code = HttpStatusCode.BadRequest;
                };

                if (code >= HttpStatusCode.InternalServerError)
                {
                    errorMessage = exception.InnerException != null
                        ? exception.InnerException.Message
                        : exception.Message;
                    code = HttpStatusCode.InternalServerError;
                }

                if (_appSettings.IsWriteLog)
                {
                    WriteConsole(context.TraceIdentifier, context.Request.Method, GetPath(context), bodyRequestText, errorMessage);
                }
                var result = string.Empty;

                var errorCode = (int?)exception.Data[HmsException.ErrorCode] ?? 0;

                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };

                result = JsonConvert.SerializeObject(
                    errorCode > 0 ? new ErrorResponse(errorCode, exception.Message) : new ErrorResponse(exception.Message),
                    jsonSerializerSettings);

                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)code;

                await context.Response.WriteAsync(result);
                if (_appSettings.IsWriteLog)
                {
                    context.Response.Body.Seek(0, SeekOrigin.Begin);
                    await responseBody.CopyToAsync(originalBodyStream);
                }
                return;
            }
        }
        private double GetElapsedMilliseconds(long start, long stop)
        {
            return (stop - start) * 1000 / (double)Stopwatch.Frequency;
        }

        private string GetPath(HttpContext httpContext)
        {
            return httpContext.Features.Get<IHttpRequestFeature>()?.RawTarget ?? httpContext.Request.Path.ToString();
        }

        private void WriteConsole(string requestId, string method, string path, string bodyRequestText, string bodyResponseText)
        {
            using (LogContext.PushProperty("RequestId", requestId, true))
            using (LogContext.PushProperty("RequestMethod", method, true))
            using (LogContext.PushProperty("RequestPath", path, true))
            using (LogContext.PushProperty("RequestData", bodyRequestText, true))
            using (LogContext.PushProperty("RequestId", requestId, true))
            using (LogContext.PushProperty("ResponseData", bodyResponseText, true))
            {
                _logger.LogInformation(String.Empty);
            }
        }

        private async Task<string> GetBodyAsync(HttpContext context)
        {
            string bodyAsText = await new StreamReader(context.Request.Body).ReadToEndAsync();
            context.Request.Body = GetMemoryStream(bodyAsText);
            return bodyAsText;
        }
        private MemoryStream GetMemoryStream(string text)
        {
            var injectedStream = new MemoryStream();
            var bytesToWrite = Encoding.UTF8.GetBytes(text);
            injectedStream.Write(bytesToWrite, 0, bytesToWrite.Length);
            injectedStream.Seek(0, SeekOrigin.Begin);
            return injectedStream;
        }
    }
}