﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;
using System.Text;
using Hms.Project.Api.Helpers;
using Hms.Project.Api.Middleware;
using Hms.Project.Data;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Interfaces.AzureUploadFile;
using Hms.Project.Service.Models;
using Hms.Project.Service.Models.AzureUploadFile;
using Hms.Project.Service.Repositories;
using Hms.Project.Service.Services;
using Hms.Project.Service.Services.AzureUploadFile;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog;

namespace Hms.Project.Api
{
    public partial class Startup
    {
        private const string DataProjectAssemblyName = "Hms.Project.Data";
        private const string DefaultConnection = "DefaultConnection";
        private const string AppSettings = "AppSettings";
        private ILoggerFactory _loggerFactory;
        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(m =>
                m.UseMySql(
                    Configuration.GetConnectionString(DefaultConnection),
                    b => b.MigrationsAssembly(DataProjectAssemblyName)));

            services.AddIdentity<User, Role>(ConfigureIdentity)
                .AddEntityFrameworkStores<DataContext>()
                .AddDefaultTokenProviders();
            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromDays(1);
            });

            services.AddHealthChecks();
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddMvcOptions(options =>
                {
                    options.AllowCombiningAuthorizeFilters = false;
                });

            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });

            services.Configure<ApiBehaviorOptions>(opt => { opt.SuppressModelStateInvalidFilter = true; });

            // === Application Setting ===
            var appSettingsSection = Configuration.GetSection(AppSettings);
            services.Configure<AppSettings>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.JWT.Secret) ??
                      throw new ArgumentNullException("Cannot find `Secret Setting`");
            services.AddSingleton<AppSettings>(appSettings);

            services.AddAuthentication(x =>
              {
                  x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                  x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
              })
                  .AddJwtBearer(x =>
                  {
                      x.RequireHttpsMetadata = false;
                      x.Events = new JwtBearerEvents { OnTokenValidated = OnTokenValidated };
                      x.SaveToken = true;
                      x.TokenValidationParameters = new TokenValidationParameters
                      {
                          NameClaimType = JwtRegisteredClaimNames.Sub,
                          ValidateIssuerSigningKey = true,
                          IssuerSigningKey = new SymmetricSecurityKey(key),
                          ValidateIssuer = false,
                          ValidateAudience = false,
                      };
                  });

            // === Swagger ===
            services.AddSwaggerGen(ConfigureSwagger);

            // === Default Root ===
            IFileProvider physicalProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            IFileProvider embeddedProvider = new EmbeddedFileProvider(Assembly.GetEntryAssembly());
            IFileProvider compositeProvider = new CompositeFileProvider(physicalProvider, embeddedProvider);

            services.AddSingleton<IFileProvider>(compositeProvider);
            // === Container ===
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserLogsService, UserLogsService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<ILanguageService, LanguageService>();
            services.AddScoped<IAzureBlobStorageService>(factory =>
            {
                return new AzureBlobStorageService(new AzureBlobSetings(
                    storageAccount: appSettings.AzureBlobStorage.Blob_StorageAccount,
                    storageKey: appSettings.AzureBlobStorage.Blob_StorageKey,
                    containerName: appSettings.AzureBlobStorage.Blob_ContainerName));
            });
            services.AddScoped<IAuditService, AuditService>();
            services.AddScoped<ISendOTPService, SendOTPService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //[Obsolete]
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory log)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();

            log.AddSerilog();
            if (env.IsDevelopment() || env.EnvironmentName == "local")
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHealthChecks("/healthcheck");
            app.UseAuthentication();

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .SetPreflightMaxAge(TimeSpan.FromSeconds(600)));
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Hms Netcore Api");
            });

            AutoMapperConfig.Initialize();
        }
    }
}