using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Hms.Project.Api.Helpers;
using Hms.Project.Api.Models.Requests;
using Hms.Project.Api.Models.Responses;
using Hms.Project.Common;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Common;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Hms.Project.Api.Controllers
{
    public class UserController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly string _clientIp;
        private readonly AppSettings _appSettings;

        public UserController(IUserService userService,
            IOptions<AppSettings> appSettings,
            IHttpContextAccessor accessor)
        {
            _userService = userService;
            _appSettings = appSettings.Value;
            _clientIp = accessor?.HttpContext?.Connection?.RemoteIpAddress?.ToString() ??
                        HttpContext.Request.Headers["X-Forwarded-For"].ToString().Split(',').FirstOrDefault() ??
                        string.Empty;
        }

        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="updateUserRequest"></param>
        /// <returns>201</returns>
        [HttpPost]
        [HasPermission(Permissions.UserCreate)]
        public async Task<IActionResult> CreateUser([FromBody] UpdateUserRequest updateUserRequest)
        {
            var user = Mapper.Map<User>(updateUserRequest);
            user.IsSystemUser = true;
            var userRequestModel = new UserRequestModel
            {
                User = user,
                Password = updateUserRequest.Password,
                RoleId = updateUserRequest.RoleId,
            };

            await _userService.Create(userRequestModel, createdBy: GetUserId());

            return Ok(new SuccessResponse<bool> { Data = true });
        }

        /// <summary>
        /// Get User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HasPermission(Permissions.UserView)]
        [HttpGet("{id}")]
        public async Task<ActionResult> GetUser([FromRoute] Guid id)
        {
            var user = await _userService.GetOne(id);
            var userResponse = Mapper.Map<UserResponse>(user);

            return Ok(new SuccessResponse<UserResponse>(userResponse));
        }

        /// <summary>
        /// Get Users
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [HasPermission(Permissions.UserView)]
        public async Task<ActionResult<PagingResult<UserResponse>>> GetUsers([FromQuery] UserFilterQuery query)
        {
            var users = await _userService.GetList(query);
            var userResponse = Mapper.Map<PagingResult<UserResponse>>(users);

            return Ok(userResponse);
        }

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="updateUserRequest">UpdateUserRequest</param>
        /// <param name="id">User Id</param>
        /// <returns>204</returns>
        [HttpPut("{id}")]
        [HasPermission(Permissions.UserUpdate)]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUserRequest updateUserRequest, [FromRoute] Guid id)
        {
            var updatedBy = GetUserId();
            var user = Mapper.Map<User>(updateUserRequest);
            user.Id = id;
            var userRequestModel = new UserRequestModel
            {
                User = user,
                Password = updateUserRequest.Password,
                RoleId = updateUserRequest.RoleId,
                IsLocked = updateUserRequest.IsLocked
            };

            await _userService.Update(userRequestModel, updatedBy);

            return Ok(new SuccessResponse<bool> { Data = true });
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [HasPermission(Permissions.UserDelete)]
        public async Task<IActionResult> DeleteUser([FromRoute] Guid id)
        {
            var deletedBy = GetUserId();
            await _userService.Delete(id, deletedBy: deletedBy);

            return Ok(new SuccessResponse<bool> { Data = true });
        }

        /// <summary>
        /// Get profile
        /// </summary>
        /// <returns></returns>
        [HttpGet("Profile")]
        public async Task<ActionResult> GetProfile()
        {
            var user = await _userService.GetOne(GetUserId(), true);
            var userResponse = Mapper.Map<UserResponse>(user);

            return Ok(new SuccessResponse<UserResponse>(userResponse));
        }

        /// <summary>
        /// Update Profile
        /// </summary>
        /// <param name="updateProfileRequest"></param>
        /// <returns></returns>
        [HttpPut("Profile")]
        public async Task<IActionResult> UpdateProfile([FromBody] UpdateProfileRequest updateProfileRequest)
        {
            var user = Mapper.Map<User>(updateProfileRequest);
            user.Id = GetUserId();
            var userRequestModel = new ProfileRequestModel
            {
                User = user,
                Password = updateProfileRequest.Password,
                OldPassword = updateProfileRequest.OldPassword,
            };

            await _userService.UpdateProfile(userRequestModel);

            return Ok(new SuccessResponse<bool> { Data = true });
        }

        /// <summary>
        /// Login.
        /// </summary>
        /// <param name="request">Username and password.</param>
        /// <returns>Token value.</returns>
        [AllowAnonymous]
        [TypeFilter(typeof(LogIpAttribute))]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] SignInRequest request)
        {
            if (_appSettings.OTP.LoginWithTwoFactorAuthenticator)
            {
                throw new HmsException(UserIssue.InvalidLoginMethod);
            }

            if (!await GoogleReCaptcha.IsReCaptchaPassedAsync(request.GoogleCaptchaResponse,
                _appSettings.Recaptcha.SecretKey, _appSettings.Recaptcha.Endpoint))
            {
                throw new HmsException(UserIssue.InvalidCaptcha);
            }

            var signInModel = new SignInModel
            {
                Username = request.Username,
                Password = request.Password,
                DeviceId = request.DeviceId,
                ClientIp = _clientIp
            };
            var userLoginAccessToken = await _userService.Authenticate(signInModel);

            return Ok(userLoginAccessToken);
        }

        /// <summary>
        /// Logout.
        /// </summary>
        /// <returns>204</returns>
        [Authorize]
        [HttpGet("Logout")]
        public async Task<IActionResult> Logout()
        {
            var userId = GetUserId();
            await _userService.Logout(GetTokenSession(), userId, _clientIp);

            return Ok(new SuccessResponse<bool> { Data = true });
        }

        [AllowAnonymous]
        [HttpPost("Refresh")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest request)
        {
            var tokenResult = await _userService.RefreshToken(request.RefreshToken);

            return Ok(tokenResult);
        }
        
        /// <summary>
        /// Login With Two Factor Authenticator Method.
        /// </summary>
        /// <param name="request">Username and password.</param>
        /// <returns>Token value.</returns>
        [AllowAnonymous]
        [HttpPost("LoginWithTwoFactorAuthenticator")]
        public async Task<IActionResult> LoginWithTwoAuthenticator([FromBody] SignInWithTwoFactorRequest request)
        {
            if (!_appSettings.OTP.LoginWithTwoFactorAuthenticator)
            {
                throw new HmsException(UserIssue.InvalidLoginMethod);
            }

            if (!await GoogleReCaptcha.IsReCaptchaPassedAsync(request.GoogleCaptchaResponse,
                _appSettings.Recaptcha.SecretKey, _appSettings.Recaptcha.Endpoint))
            {
                throw new HmsException(UserIssue.InvalidCaptcha);
            }

            var signInWithTwoFactorModel = new SignInWithTwoFactorModel
            {
                Username = request.Username,
                Password = request.Password,
            };
            var userVerifyGuiId = await _userService.AuthenticateWithTwoFactorAuthenticator(signInWithTwoFactorModel);
            if (userVerifyGuiId != Guid.Empty)
            {
                return Ok(new SuccessResponse<Guid> { Data = userVerifyGuiId });
            }
            return NotFound();
        }

        /// <summary>
        /// Login With Two Factor Authenticator Method.
        /// </summary>
        /// <param name="request">Username and password.</param>
        /// <returns>Token value.</returns>
        [AllowAnonymous]
        [TypeFilter(typeof(LogIpAttribute))]
        [HttpPost("VerifyOTPCode")]
        public async Task<IActionResult> VerifyOTPCode([FromBody] VerifyOTPRequest request)
        {
            var verifyOTPModel = new VerifyOTPModel
            {
                VerifyCodeId = request.VerifyCodeId,
                VerifyCode = request.VerifyCode,
                DeviceId = request.DeviceId,
                ClientIp = _clientIp
            };
            var userLoginAccessToken = await _userService.VerifyOTPCode(verifyOTPModel);

            return Ok(userLoginAccessToken);
        }
    }
}