﻿using System.Threading.Tasks;
using AutoMapper;
using Hms.Project.Api.Helpers;
using Hms.Project.Api.Models.Requests;
using Hms.Project.Api.Models.Responses;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hms.Project.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguageController : ControllerBase
    {
        private readonly ILanguageService _languageService;

        public LanguageController(ILanguageService languageService)
        {
            _languageService = languageService;
        }

        [HttpGet]
        [TypeFilter(typeof(LogIpAttribute))]
        public async Task<ActionResult<PagingResult<LanguageItemResponse>>> Get([FromQuery] LanguageFilterQuery query)
        {
            var value = await _languageService.GetAll(query);
            var data = Mapper.Map<PagingResult<LanguageItemResponse>>(value);

            return Ok(data);
        }

        [AllowAnonymous]
        [HttpGet("/Choose/{language}")]
        public IActionResult Get(string language)
        {
            string value = _languageService.ReadJsonOject(language);
            return Ok(new SuccessResponse<string>(value));
        }

        [HttpPut]
        public async Task<IActionResult> Put(LanguageRequest request)
        {
            var langauge = Mapper.Map<LanguageModel>(request);
            langauge = await _languageService.Update(langauge);
            return Ok(new SuccessResponse<LanguageResponse>(Mapper.Map<LanguageResponse>(langauge)));
        }
    }
}