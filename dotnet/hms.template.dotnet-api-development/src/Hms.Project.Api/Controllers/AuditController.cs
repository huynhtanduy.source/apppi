using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Hms.Project.Api.Models.Responses;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hms.Project.Api.Controllers
{
    public class AuditController : BaseApiController
    {
        private readonly IAuditService _auditService;

        public AuditController(IAuditService auditService)
        {
            _auditService = auditService;
        }
        
        /// <summary>
        /// Get Audits
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<PagingResult<GetAuditPagingResultResponse>>> GetPaging([FromQuery] AuditFilterQuery query)
        {
            var audits = await _auditService.GetPaging(query);
            var auditResponse = Mapper.Map<PagingResult<GetAuditPagingResultResponse>>(audits);

            return Ok(auditResponse);
        }
        
        /// <summary>
        /// Get Audit details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult> GetAuditById([FromRoute] Guid id)
        {
            var auditDetails = await _auditService.GetAuditById(id);
            if (auditDetails != null)
            {
                var auditDetailsResponse = Mapper.Map<List<GetAuditDetailsResultResponse>>(auditDetails);

                return Ok(new SuccessResponse<List<GetAuditDetailsResultResponse>>(auditDetailsResponse));
            }
            return NotFound();
        }
    }
}