﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Hms.Project.Api.Helpers;
using Hms.Project.Api.Models.Requests;
using Hms.Project.Api.Models.Responses;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Hms.Project.Api.Controllers
{
    public class RoleController : BaseApiController
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// Create Role
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [HasPermission(Permissions.RoleCreate)]
        public async Task<IActionResult> Create([FromBody] RoleRequest request)
        {
            var role = Mapper.Map<Role>(request);
            var roleRequestModel = new RoleRequestModel
            {
                Role = role,
                Permissions = request.Permissions,
            };
            await _roleService.Create(roleRequestModel, GetUserId());

            return Ok(new SuccessResponse<bool> { Data = true });
        }

        /// <summary>
        /// Get Role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [HasPermission(Permissions.RoleView)]
        public async Task<ActionResult> GetRole([FromRoute] Guid id)
        {
            var role = await _roleService.GetOne(id);
            var roleResponse = Mapper.Map<RoleDetailResponse>(role);

            return Ok(new SuccessResponse<RoleDetailResponse>(roleResponse));
        }

        /// <summary>
        /// Get Roles
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [HasPermission(Permissions.RoleView)]
        public async Task<ActionResult<PagingResult<RoleResponse>>> GetRoles([FromQuery] RoleFilterQuery query)
        {
            var roles = await _roleService.GetList(query);
            var roleResponse = Mapper.Map<PagingResult<RoleResponse>>(roles);

            return Ok(roleResponse);
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="roleRequest"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [HasPermission(Permissions.RoleUpdate)]
        public async Task<IActionResult> Update([FromBody] RoleRequest roleRequest, [FromRoute] Guid id)
        {
            var role = Mapper.Map<Role>(roleRequest);
            role.Id = id;
            var roleRequestModel = new RoleRequestModel
            {
                Role = role,
                Permissions = roleRequest.Permissions,
            };

            await _roleService.Update(roleRequestModel, GetUserId());

            return Ok(new SuccessResponse<bool> { Data = true });
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [HasPermission(Permissions.RoleDelete)]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            await _roleService.Delete(id);

            return Ok(new SuccessResponse<bool> { Data = true });
        }

        /// <summary>
        /// Get Permission
        /// </summary>
        /// <returns></returns>
        [HasPermission(Permissions.RoleView)]
        [HttpGet("Permission")]
        public ActionResult GetPermissions()
        {
            var permissionResponse = typeof(Permissions).GetFields()
                .Select(x => x.GetValue(null)).Cast<string>().ToList();

            return Ok(new SuccessResponse<List<string>>(permissionResponse));
        }

        /// <summary>
        /// Get all Role
        /// </summary>
        /// <returns></returns>
        [HttpGet("All")]
        [HasPermission(Permissions.RoleView)]
        public async Task<ActionResult<IEnumerable<RoleResponse>>> GetRoles()
        {
            var roles = await _roleService.GetList();
            var roleResponse = Mapper.Map<List<RoleResponse>>(roles);

            return Ok(new SuccessResponse<List<RoleResponse>>(roleResponse));
        }

    }
}