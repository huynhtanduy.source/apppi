using System;
using System.Security.Claims;
using Hms.Project.Api.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;

namespace Hms.Project.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [ValidateModel]
    public class BaseApiController : ControllerBase
    {
        protected Guid GetUserId()
        {
            return User != null ? Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)) : Guid.Empty;
        }

        protected Guid GetTokenSession()
        {
            Guid.TryParse(User.FindFirstValue(JwtRegisteredClaimNames.Jti), out var tokenSession);

            return tokenSession;
        }
    }
}