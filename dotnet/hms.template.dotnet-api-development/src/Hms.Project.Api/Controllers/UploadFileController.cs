﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hms.Project.Api.Models.Requests.UploadFile;
using Hms.Project.Api.Models.Responses.UploadFile;
using Hms.Project.Service.Interfaces.AzureUploadFile;
using Hms.Project.Service.Models.AzureUploadFile;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Hms.Project.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadFileController : ControllerBase
    {
        private static IAzureBlobStorageService _azureBlobStorage;

        public UploadFileController(IAzureBlobStorageService azureBlobStorage)
        {
            _azureBlobStorage = azureBlobStorage;
        }


        [HttpPost("create")]
        public SessionCreationStatusResponse StartSession(CreateSessionParamsRequest data)
        {

            SessionFile session = _azureBlobStorage.CreateSessionFile(data.FileName,
                                                          data.ChunkSize.Value,
                                                          data.TotalSize.Value);

            return SessionCreationStatusResponse.FromSession(session);
        }

        [HttpPut("uploadazure/session/{sessionId}/")]
        [Produces("application/json")]
        [Consumes("multipart/form-data")]
        public async Task<JsonResult> UploadFileChunkAzureAsync([FromRoute]string sessionId,
                                      [FromQuery] int? chunkNumber,
                                      [FromForm] IFormFile inputFile)
        {
            string link = "";
            try
            {
                IFormFile file = inputFile ?? Request.Form.Files.First();
                SessionFile session = _azureBlobStorage.GetSessionFile(sessionId);
                var blockId = Convert.ToBase64String(Encoding.UTF8.GetBytes(chunkNumber?.ToString()));
                var fileStream = file.OpenReadStream();
                link = await _azureBlobStorage.UploadBlockAsync(session.FileInfo.FileName, blockId, fileStream);
                session.BlockIds.Add(blockId);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
            return new JsonResult(link);
        }

        [HttpPost("uploadAzure/complete")]
        [Produces("application/json")]
        public async Task<IActionResult> UploadAzureAsync(UploadFileAzureRequest request)
        {
            SessionFile session = _azureBlobStorage.GetSessionFile(request.SessionId);
            var link = "";
            try
            {
                link = await _azureBlobStorage.CommitBlockAsync(session.FileInfo.FileName, session.BlockIds);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
            return Ok(link);
        }



    }


}