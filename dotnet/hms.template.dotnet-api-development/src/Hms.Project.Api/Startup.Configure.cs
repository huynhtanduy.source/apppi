﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Hms.Project.Api.Helpers;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Hms.Project.Api
{
    public partial class Startup
    {
        private static void ConfigureSwagger(SwaggerGenOptions options)
        {
            options.SwaggerDoc("v1", new Info
            {
                Title = "HmsProjectApi",
                Version = "v1",
                Description = "HMS Project NetCore API template",
            });
            options.AddSecurityDefinition(
                "Bearer",
                new ApiKeyScheme
                {
                    In = "header",
                    Description = "Please enter JWT with Bearer into field",
                    Name = "Authorization",
                });
            options.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
            {
                { "Bearer", Enumerable.Empty<string>() },
            });
            options.OperationFilter<SwaggerFileUpload>();
        }

        private static void ConfigureIdentity(IdentityOptions options)
        {
            options.SignIn.RequireConfirmedEmail = true;
            options.User.RequireUniqueEmail = true;

            // Default Password settings.
            options.Password.RequireDigit = false;
            options.Password.RequireLowercase = false;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireUppercase = false;
            options.Password.RequiredLength = 6;
            options.Password.RequiredUniqueChars = 0;

            // Default Lockout settings.
            options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
            options.Lockout.MaxFailedAccessAttempts = 5;
            options.Tokens.EmailConfirmationTokenProvider = TokenOptions.DefaultEmailProvider;
            options.Tokens.PasswordResetTokenProvider = TokenOptions.DefaultEmailProvider;
        }

        private static async Task OnTokenValidated(TokenValidatedContext context)
        {
            var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
            var userManager = context.HttpContext.RequestServices.GetRequiredService<UserManager<User>>();
            var roleManager = context.HttpContext.RequestServices.GetRequiredService<RoleManager<Role>>();

            Guid.TryParse(context.Principal.FindFirstValue(JwtRegisteredClaimNames.Jti), out var tokenSession);

            var user = await userManager.GetUserAsync(context.Principal);

            if (user == null)
            {
                context.Fail("Unauthorized");
            }

            var userSession = await userService.GetUserSession(user.Id, tokenSession);
            if (userSession == null)
            {
                context.Fail("Unauthorized");
            }

            var roles = userManager.GetRolesAsync(user).Result;
            var roleClaims = new List<Claim>();
            foreach (var roleName in roles)
            {
                var role = roleManager.FindByNameAsync(roleName).Result;
                var claims = roleManager.GetClaimsAsync(role).Result.Where(c => c.Type == CustomClaimTypes.Permission).ToList();
                roleClaims.AddRange(claims);
            }

            var claimsIdentity = new ClaimsIdentity(roleClaims);
            context.Principal.AddIdentity(claimsIdentity);
        }
    }
}