using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Tokens;

namespace Hms.Project.Api.Helpers
{
    public class HasPermissionAttribute : AuthorizeAttribute, IAsyncAuthorizationFilter
    {
        private readonly string[] _permissions;

        public HasPermissionAttribute(params string[] permissions)
        {
            _permissions = permissions;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var userPermissionClaims = context.HttpContext.User.FindAll(CustomClaimTypes.Permission).ToList();
            if (!userPermissionClaims.Any(p => _permissions.Contains(p.Value)))
            {
                context.Result = new ForbidResult();
            }

            await Task.CompletedTask;
        }
    }
}