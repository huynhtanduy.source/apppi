using AutoMapper;
using Hms.Project.Api.Models.Requests;
using Hms.Project.Api.Models.Responses;
using Hms.Project.Data.Entities;
using Hms.Project.Service.Models;
using Microsoft.EntityFrameworkCore;

namespace Hms.Project.Api.Helpers
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<UpdateUserRequest, User>().ReverseMap();
                cfg.CreateMap<UpdateProfileRequest, User>().ReverseMap();
                cfg.CreateMap<LanguageRequest, LanguageModel>().ReverseMap();
                cfg.CreateMap<RoleRequest, Role>().ReverseMap();
                cfg.CreateMap<RoleDetailModel, RoleDetailResponse>().ReverseMap();
                cfg.CreateMap<UserLogsModel, UserLog>().ReverseMap();
                cfg.CreateMap<DbLoggerCategory.Update, User>().ReverseMap();

                cfg.CreateMap<UserResponse, UserModel>().ReverseMap();
                cfg.CreateMap<RoleResponse, RoleModel>().ReverseMap();
                cfg.CreateMap<RoleDetailResponse, RoleDetailModel>().ReverseMap();
                cfg.CreateMap<LanguageResponse, LanguageModel>().ReverseMap();
                cfg.CreateMap<LanguageItemResponse, LanguageItemModel>().ReverseMap();
                cfg.CreateMap<GetAuditPagingResultResponse, GetAuditPagingResultModel>().ReverseMap();
                cfg.CreateMap<GetAuditDetailsResultResponse, GetAuditDetailsResultModel>().ReverseMap();
                
                cfg.CreateMap(typeof(PagingResult<>), typeof(PagingResult<>)).ReverseMap();
            });
        }
    }
}