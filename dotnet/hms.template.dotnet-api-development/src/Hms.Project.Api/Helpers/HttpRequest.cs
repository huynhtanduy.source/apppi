using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Hms.Project.Common.Extensions;
using Hms.Project.Service.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Hms.Project.Api.Helpers
{
    public class HttpRequest
    {
        private static string _apiBasicUri;
        
        public HttpRequest(IOptions<AppSettings> appSettings)
        {
            _apiBasicUri = appSettings.Value.ApiBasicUrl;
        }
        
        public static async Task Post<T>(string url, T contentValue)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiBasicUri);
                var content = new StringContent(contentValue.ToJson(), Encoding.UTF8, "application/json");
                var result = await client.PostAsync(url, content);
                result.EnsureSuccessStatusCode();
            }
        }

        public static async Task Put<T>(string url, T stringValue)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiBasicUri);
                var content = new StringContent(stringValue.ToJson(), Encoding.UTF8, "application/json");
                var result = await client.PutAsync(url, content);
                result.EnsureSuccessStatusCode();
            }
        }

        public static async Task<T> Get<T>(string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiBasicUri);
                var result = await client.GetAsync(url);
                result.EnsureSuccessStatusCode();
                string resultContentString = await result.Content.ReadAsStringAsync();
                T resultContent = JsonConvert.DeserializeObject<T>(resultContentString);
                return resultContent;
            }
        }

        public static async Task Delete(string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiBasicUri);
                var result = await client.DeleteAsync(url);
                result.EnsureSuccessStatusCode();
            }
        }
    }
}