﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Hms.Project.Api.Helpers
{
    public class GoogleReCaptcha
    {
        public static async Task<bool> IsReCaptchaPassedAsync(string gRecaptchaResponse, string secret, string endPoint)
        {
            var httpClient = new HttpClient();
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("secret", secret),
                new KeyValuePair<string, string>("response", gRecaptchaResponse)
            });
            var res = await httpClient.PostAsync(endPoint, content);
            if (res.StatusCode != HttpStatusCode.OK)
            {
                return false;
            }

            var jsonRes = res.Content.ReadAsStringAsync().Result;
            dynamic jsonData = JObject.Parse(jsonRes);

            return jsonData.success == "true";
        }
    }
}
