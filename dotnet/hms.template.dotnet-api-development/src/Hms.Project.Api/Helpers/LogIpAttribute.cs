﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Hms.Project.Common;
using Hms.Project.Service.Common;
using Hms.Project.Service.Interfaces;
using Hms.Project.Service.Models;
using Hms.Project.Service.Queries;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using static Hms.Project.Service.Common.Enumerations;

namespace Hms.Project.Api.Helpers
{
    public class LogIpAttribute : ActionFilterAttribute
    {
        private readonly IUserLogsService _userLogsService;
        private readonly AppSettings _appSettings;

        public LogIpAttribute(IUserLogsService userLogsService,
            IOptions<AppSettings> appSettings)
        {
            _userLogsService = userLogsService;
            _appSettings = appSettings.Value;

        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Guid? userId = null;
            try
            {
                if (context.HttpContext.User != null && context.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier) != null)
                    userId = Guid.Parse(context.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            }
            catch { }
            string url = Microsoft.AspNetCore.Http.Extensions.UriHelper.GetEncodedPathAndQuery(context.HttpContext.Request);
            var remoteIp = context.HttpContext.Connection.RemoteIpAddress.ToString();
            var flag = await LogUserActivity(userId, url, remoteIp);
            if (flag)
                await next();
            else
            {
                throw new HmsException(CommonIssue.IPErrorCode);
            }
        }

        private async Task<bool> LogUserActivity(Guid? userId, string url, string clientIp)
        {
            await _userLogsService.Create(new UserLogsModel
            {
                ClientIp = clientIp,
                Id = Guid.NewGuid(),
                Link = url,
                UserId = userId,
                Type = UserLogType.LimitIP.GetHashCode(),
            });

            var listUserLogs = await _userLogsService.GetAll(new UserLogFilterQuery()
            {
                Link = url,
                ClientIp = clientIp,
                Time = _appSettings.RateLimit.Time,
                Type = UserLogType.LimitIP.GetHashCode(),
            });
            return (listUserLogs.Count <= _appSettings.RateLimit.Count);
        }

    }
}