using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Hms.Project.Api.Helpers
{
    public class SwaggerFileUpload : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.OperationId.ToLower() != "upload")
            {
                return;
            }

            operation.Parameters.Clear();
            operation.Parameters.Add(new NonBodyParameter
            {
                Name = "file",
                In = "formData",
                Description = "Upload File",
                Required = true,
                Type = "file",
            });

            operation.Parameters.Add(new BodyParameter
            {
                Name = "directory",
                In = "formData",
                Description = "Directory",
                Required = true,
            });

            operation.Consumes.Add("multipart/form-data");
        }
    }
}