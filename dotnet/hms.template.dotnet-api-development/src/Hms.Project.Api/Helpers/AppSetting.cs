using System.ComponentModel.DataAnnotations;

namespace Hms.Project.Api.Helpers
{
    public class AppSetting
    {
        [Required]
        public string JwtSecret { get; set; }

        [Required]
        public string JwtIssuer { get; set; }

        [Required]
        public string JwtAudience { get; set; }

        [Required]
        public int JwtExpireDays { get; set; }
    }

}