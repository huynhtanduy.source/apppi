namespace Hms.Project.Api.Helpers
{
    public static class Permissions
    {
        // Users
        public const string UserCreate = "users.create";
        public const string UserUpdate = "users.update";
        public const string UserDelete = "users.delete";
        public const string UserView = "users.view";

        // Roles
        public const string RoleCreate = "roles.create";
        public const string RoleUpdate = "roles.update";
        public const string RoleDelete = "roles.delete";
        public const string RoleView = "roles.view";
    }
}