using System.IO;
using System.Threading.Tasks;
using NVelocity;
using NVelocity.App;
using NVelocity.Runtime;
using Hms.Project.Service.Models;
using Microsoft.Extensions.Options;

namespace Hms.Project.Api.Helpers
{
    public class BuildTemplateWithData
    {
        private static string _templatePath;
        
        public BuildTemplateWithData(IOptions<AppSettings> appSettings)
        {
            _templatePath = appSettings.Value.TemplateUrl;
        }
        
        public static async Task<string> RenderHTML (string templateName, object data)
        {
            var vltEngine = new VelocityEngine();
            vltEngine.SetProperty(RuntimeConstants.RESOURCE_LOADER, "file");
            vltEngine.SetProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, _templatePath);
            vltEngine.Init();

            var vltTemplate = vltEngine.GetTemplate(templateName);

            var vltContext = new VelocityContext();
            vltContext.Put("data", data);

            var vltWriter = new StringWriter();
            vltTemplate.Merge(vltContext, vltWriter);
//            vltEngine.Evaluate(vltContext, vltWriter, String.Empty, templateName);


            string html = vltWriter.GetStringBuilder().ToString();
            return await Task.FromResult(html);
        }
    }
}