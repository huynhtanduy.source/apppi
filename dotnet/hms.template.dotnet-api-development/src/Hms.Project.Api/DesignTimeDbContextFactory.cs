using System.IO;
using Hms.Project.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

namespace Hms.Project.Api
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var configurationRoot = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("migrations.config.json")
                .Build();

            var builder = new DbContextOptionsBuilder<DataContext>();
            var connectionString = configurationRoot.GetConnectionString("DefaultConnection");

            builder.UseMySql(
                connectionString,
                b =>
                {
                    b.MigrationsAssembly("Hms.Project.Data");
                    b.UnicodeCharSet(CharSet.Utf8mb3);
                    b.CharSetBehavior(CharSetBehavior.AppendToUnicodeNonIndexAndKeyColumns);
                });

            return new DataContext(builder.Options);
        }
    }
}
